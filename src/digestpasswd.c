/*
 * Copyright (C) 2013, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gcrypt.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include "httpserver.h"

static struct option opt[] = {
	{"help", no_argument, NULL, 'h'},
	{"domain", required_argument, NULL, 'd'},
	{"file", required_argument, NULL, 'f'},
	{"password", required_argument, NULL, 'p'},
	{"user", required_argument, NULL, 'u'},
	{"version", no_argument, NULL, 'V'},
	{NULL, 0, NULL, 0}
};

void usage(void)
{
	fputs("usage: digestpasswd -f <file> -u <user> "
				"[-d <domain> -p password]\n", stderr);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	char *filename = NULL, *user = NULL, *domain = NULL, *password = NULL;

	opterr = 0;
	for (;;) {
		int c, index = 0;

		if ((c = getopt_long(argc, argv, "hd:f:p:u:V", opt, &index)) < 0)
			break;

		switch (c) {
			case 'h':
			default:
				usage();
				break;
			case 'd':
				domain = optarg;
				break;
			case 'f':
				filename = optarg;
				break;
			case 'p':
				password = optarg;
				break;
			case 'u':
				user = optarg;
				break;
			case 'V':
				fprintf(stderr, "digestpasswd, part of %s/%s\n\n",
					PACKAGE_NAME, PACKAGE_VERSION);
				fputs("Copyright (C) 2013,  Eric Dorino\n"
					"This is free software; see the source "
					"for copying conditions.  There is NO\n"
					"warranty; not even for MERCHANTABILITY "
					"of FITNESS FOR A PARTICULAR PURPOSE.\n",
					stderr);
				return EXIT_FAILURE;
		}
	}

	if (!user || !filename)
		usage();

	if (password && !domain)
		usage();

	if (!gcry_check_version(GCRYPT_VERSION)) {
		fputs("digestpasswd: cannot initialize libgrypt", stderr);
		return	EXIT_FAILURE;
	}
	gcry_control(GCRYCTL_DISABLE_SECMEM, 0);
	gcry_control(GCRYCTL_INITIALIZATION_FINISHED, 0);

	if (http_modify_passwords_file(filename, domain, user, password))
		return EXIT_SUCCESS;
	
	fprintf(stderr, "digestpasswd: cannot modify %s for %s:%s\n",
									filename, user, domain);
	return EXIT_FAILURE;
}
