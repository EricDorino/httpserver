/*
 * Copyright (C) 2013, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <inttypes.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include "internals.h"
#define BUFLEN	8192

static void send_ssi_file(struct http_connection *,
				const char *, FILE *, int);

static void do_ssi_include(struct http_connection *conn,
				const char *ssi, char *tag, int include_level)
{
	char file_name[BUFLEN], path[PATH_MAX], *p;
	FILE * file;

	if (sscanf(tag, " virtual=\"%[^\"]\"", file_name) == 1)
		/* File name is relative to the webserver root */
		snprintf(path, sizeof(path), "%s%c%s",
				conn->ctx->config->docroot, '/', file_name);
	else if (sscanf(tag, " file=\"%[^\"]\"", file_name) == 1)
		/* File name is relative to the webserver working directory 
		 * or it is absolute system path
		 */
		snprintf(path, sizeof(path), "%s", file_name);
	else if (sscanf(tag, " \"%[^\"]\"", file_name) == 1) {
		/* File name is relative to the currect document */
		snprintf(path, sizeof(path), "%s", ssi);
		if ((p = strrchr(path, '/')) != NULL)
			p[1] = '\0';
		snprintf(path + strlen(path),
				sizeof(path) - strlen(path), "%s", file_name);
	}
	else {
		http_error(conn, "Bad SSI #include: [%s]", tag);
		return;
	}
	if (!(file = fopen(path, "rb")))
		http_error(conn, "Cannot open SSI #include: [%s]: fopen(%s): %m", tag, path);
	else {
		if (conn->ctx->config->ssi_pattern &&
				match(conn, &conn->ctx->ssi_pattern, path))
			send_ssi_file(conn, path, file, include_level + 1);
		else
			http_send_file_data(conn, file, 0, 0, INT64_MAX);
		fclose(file);
	}
}

static void do_ssi_exec(struct http_connection *conn, char *tag)
{
	char cmd[BUFLEN];
	FILE *file;

	if (sscanf(tag, " \"%[^\"]\"", cmd) != 1)
		http_error(conn, "Bad SSI #exec: [%s]", tag);
	else if (!(file = popen(cmd, "r")))
		http_error(conn, "Cannot SSI #exec: [%s]: %m", cmd);
	else {
		http_send_file_data(conn, file, 0, 0, INT64_MAX);
		pclose(file);
	}
}

static void send_ssi_file(struct http_connection *conn,
				const char *path, FILE *file, int include_level)
{
	char buf[BUFLEN];
	int ch, len, in_ssi_tag;

	if (include_level > 10) {
		http_error(conn, "SSI #include level is too deep (%s)", path);
		return;
	}

	in_ssi_tag = len = 0;
	while ((ch = fgetc(file)) != EOF) {
		if (in_ssi_tag && ch == '>') {
			in_ssi_tag = 0;
			buf[len++] = (char) ch;
			buf[len] = '\0';
			assert(len <= (int) sizeof(buf));
			if (len < 6 || memcmp(buf, "<!--#", 5) != 0)
				/* Not an SSI tag, pass it */
				http_write(conn, buf, (size_t) len);
			else {
				if (!memcmp(buf + 5, "include", 7))
					do_ssi_include(conn, path, buf + 12, include_level);
				else if (!memcmp(buf + 5, "exec", 4))
					do_ssi_exec(conn, buf + 9);
				else
					http_error(conn, "%s: unknown SSI " "command: \"%s\"", path, buf);
			}
			len = 0;
		}
		else if (in_ssi_tag) {
			if (len == 5 && memcmp(buf, "<!--#", 5) != 0)
				/* Not an SSI tag */
				in_ssi_tag = 0;
			else if (len == (int) sizeof(buf) - 2) {
				http_error(conn, "%s: SSI tag is too large", path);
				len = 0;
			}
			buf[len++] = ch & 0xff;
		}
		else if (ch == '<') {
			in_ssi_tag = 1;
			if (len > 0)
				http_write(conn, buf, (size_t) len);
			len = 0;
			buf[len++] = ch & 0xff;
		}
		else {
			buf[len++] = ch & 0xff;
			if (len == (int) sizeof(buf)) {
				http_write(conn, buf, (size_t) len);
				len = 0;
			}
		}
	}
	/* Send the rest of buffered data */
	if (len > 0)
		http_write(conn, buf, (size_t) len);
}

void handle_ssi_file_request(struct http_connection *conn,
									const char *path)
{
	FILE *file;
	char buf[64];

	if (!(file = fopen(path, "rb"))) {
		http_error(conn, "Cannot open \"%s\": %m", path);
		http_send_response(conn, 500, "Cannot Open \"%s\"", path);
	}
	else {
		conn->must_close = 1;
		conn->status_code = 200;
		if (conn->ctx->config->server_software)
			http_printf(conn, "HTTP/1.1 200 OK\r\n"
				"Server: %s\r\n"
				"Date: %s\r\n"
				"Content-Type: text/html\r\n"
				"Connection: %s\r\n\r\n",
				conn->ctx->config->server_software,
				now(buf, sizeof(buf)),
				should_keep_alive(conn) ?"keep-alive" : "close");
		http_printf(conn, "HTTP/1.1 200 OK\r\n"
				"Date: %s\r\n"
				"Content-Type: text/html\r\n"
				"Connection: %s\r\n\r\n",
				now(buf, sizeof(buf)),
				should_keep_alive(conn) ?"keep-alive" : "close");
		send_ssi_file(conn, path, file, 0);
		fclose(file);
	}
}

