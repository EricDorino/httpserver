/*
 * Copyright (C) 2013, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/stat.h>
#include <assert.h>
#include <dirent.h>
#include <inttypes.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include "internals.h"
#define BUFLEN 8192

static int parse_range_header(const char *header, int64_t *a, int64_t *b)
{
	return sscanf(header, "bytes=%" PRId64 "-%" PRId64, a, b);
}

static void construct_etag(char *buf, size_t buf_len,
											time_t mtime, int64_t size) {
	snprintf(buf, buf_len, "\"%lx.%" PRId64 "\"",
		(unsigned long)mtime, size);
}

/* 
 * Send len bytes from the opened file to the client.
 */
void http_send_file_data(struct http_connection *conn, FILE *file, int64_t size,
						 int64_t offset, int64_t len)
{
	char buf[BUFLEN];
	int to_read, read, written;

	if (len > 0 && file) {
		fseeko(file, offset, SEEK_SET);
		while (len > 0) {
			/* Calculate how much to read from the file in the buffer */
			to_read = sizeof(buf);
			if ((int64_t) to_read > len)
				to_read = (int) len;

			/* Read from file, exit the loop on error */
			if ((read = fread(buf, 1, (size_t)to_read, file)) <= 0)
				break;

			/* Send read bytes to the client, exit the loop on error */
			if ((written = http_write(conn, buf, (size_t)read)) != read)
				break;

			/* Both read and were successful, adjust counters */
			conn->bytes_sent += written;
			len -= written;
		}
	}
}

void handle_file_request(struct http_connection *conn,
				const char *path, time_t mtime, int64_t size)
{
	char date[64], lm[64], etag[64], range[64];
	const char *msg = "OK", *hdr;
	time_t curtime = time(NULL);
	FILE *file;
	int64_t cl, r1, r2;
	const char *mime;
	int n;

	mime = http_get_mime_type(path);
	cl = size;
	conn->status_code = 200;
	range[0] = '\0';

	if (!(file = fopen(path, "rb"))) {
		http_error(conn, "Cannot open file \"%s\": %m", path);
		http_send_response(conn, 500, "Cannot Open File \"%s\"", path);
		return;
	}

	/* If Range: header specified, act accordingly */
	r1 = r2 = 0;
	hdr = http_get_header(conn, "Range");
	if (hdr != NULL &&
			(n = parse_range_header(hdr, &r1, &r2)) > 0 &&
			r1 >= 0 && r2 >= 0) {
		conn->status_code = 206;
		cl = n == 2 ? (r2 > cl ? cl : r2) - r1 + 1: cl - r1;
		snprintf(range, sizeof(range),
				"Content-Range: bytes "
				"%" PRId64 "-%" PRId64 "/%" PRId64 "\r\n",
				r1, r1 + cl - 1, size);
		msg = "Partial Content";
	}

	/* 
	 * Prepare Etag, Date, Last-Modified headers. Must be in UTC, according 
	 * to http://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html#sec3.3
	 */
	gmt_date_string(date, sizeof(date), &curtime);
	gmt_date_string(lm, sizeof(lm), &mtime);
	construct_etag(etag, sizeof(etag), mtime, size);

	if (conn->ctx->config->server_software)
		http_printf(conn,
			"HTTP/1.1 %d %s\r\n"
			"Server: %s\r\n"
			"Date: %s\r\n"
			"Last-Modified: %s\r\n"
			"Etag: %s\r\n"
			"Content-Type: %.*s\r\n"
			"Content-Length: %" PRId64 "\r\n"
			"Connection: %s\r\n"
			"Accept-Ranges: bytes\r\n"
			"%s\r\n",
			conn->status_code, msg, conn->ctx->config->server_software,
			date, lm, etag, strlen(mime),
			mime, cl, should_keep_alive(conn) ?"keep-alive" : "close",
			range);
	else
		http_printf(conn,
			"HTTP/1.1 %d %s\r\n"
			"Date: %s\r\n"
			"Last-Modified: %s\r\n"
			"Etag: %s\r\n"
			"Content-Type: %.*s\r\n"
			"Content-Length: %" PRId64 "\r\n"
			"Connection: %s\r\n"
			"Accept-Ranges: bytes\r\n"
			"%s\r\n",
			conn->status_code, msg,
			date, lm, etag, strlen(mime),
			mime, cl, should_keep_alive(conn) ?"keep-alive" : "close",
			range);

	if (strcmp(conn->request.method, "HEAD") != 0)
		http_send_file_data(conn, file, size, r1, cl);
	fclose(file);
}


void http_send_file(struct http_connection *conn, const char *path) {
	struct stat st;

	if (stat(path, &st) == 0 && !S_ISDIR(st.st_mode))
		handle_file_request(conn, path, st.st_mtime, st.st_size);
	else
		http_send_response(conn, 404, "Not found");
}


/*
 * Return True if we should reply 304 Not Modified.
 */

int is_not_modified(const struct http_connection *conn,
					 time_t mtime, int64_t size) {
	char etag[64];
	const char *ims = http_get_header(conn, "If-Modified-Since");
	const char *inm = http_get_header(conn, "If-None-Match");

	construct_etag(etag, sizeof(etag), mtime, size);
	return (inm != NULL && !strcasecmp(etag, inm)) ||
		(ims != NULL && mtime <= parse_date_string(ims));
}


struct de {
	struct http_connection *conn;
	char *file_name;
	int is_dir;
	time_t mtime;
	int64_t size;
};

static void print_dir_entry(struct de *de)
{
	char size[64], mod[64], href[PATH_MAX];

	if (de->is_dir)
		snprintf(size, sizeof(size), "%s", "[DIRECTORY]");
	else {
		if (de->size < 1024)
			snprintf(size, sizeof(size), "%d", (int) de->size);
		else if (de->size < 0x100000)
			snprintf(size, sizeof(size),
					"%.1fk", (double) de->size / 1024.0);
		else if (de->size < 0x40000000)
			snprintf(size, sizeof(size),
					"%.1fM", (double) de->size / 1048576);
		else
			snprintf(size, sizeof(size),
					"%.1fG", (double) de->size / 1073741824);
	}
	strftime(mod, sizeof(mod), "%d-%b-%Y %H:%M",
				 localtime(&de->mtime));
	url_encode(de->file_name, href, sizeof(href), NULL);
	de->conn->bytes_sent += http_printf(de->conn,
			"<tr><td><a href=\"%s%s%s\">%s%s</a></td>"
			"<td>&nbsp;%s</td><td>&nbsp;&nbsp;%s</td></tr>\n",
			de->conn->request.uri, href, de->is_dir ? "/" : "",
			de->file_name, de->is_dir ? "/" : "", mod, size);
}

static int compare_dir_entries(const void *p1, const void *p2) {
	const struct de *a = (const struct de *)p1,
					*b = (const struct de *)p2;
	const char *query_string = a->conn->request.query_string;
	int cmp_result = 0;

	if (!query_string)
		query_string = "na";

	if (a->is_dir && !b->is_dir)
		return -1;	/* Always put directories on top */
	else if (!a->is_dir && b->is_dir)
		return 1;	/* Always put directories on top */
	else if (*query_string == 'n')
		cmp_result = strcmp(a->file_name, b->file_name);
	else if (*query_string == 's')
		cmp_result = a->size == b->size ? 0 :
			a->size > b->size ? 1 : -1;
	else if (*query_string == 'd')
		cmp_result = a->mtime == b->mtime ? 0 :
			a->mtime > b->mtime ? 1 : -1;

	return query_string[1] == 'd' ? -cmp_result : cmp_result;
}

static int scan_directory(struct http_connection *conn,
				const char *dir, void *data, void (*cb)(struct de *, void *))
{
	char path[PATH_MAX];
	struct dirent *dp;
	DIR *dirp;
	struct de de;
	struct stat st;

	if (!(dirp = opendir(dir)))
		return 0;
	else {
		de.conn = conn;

		while ((dp = readdir(dirp)) != NULL) {
			/* Do not show current dir and hidden files */
			if (!strcmp(dp->d_name, ".") ||
					!strcmp(dp->d_name, "..") ||
					(conn->ctx->config->hide_files && 
					 match(conn, &conn->ctx->hide_files, dp->d_name))) {
				continue;
			}

			snprintf(path, sizeof(path), "%s%c%s", dir, '/', dp->d_name);

			if (stat(path, &st) == 0) {
				de.is_dir = S_ISDIR(st.st_mode);
				de.mtime = st.st_mtime;
				de.size = st.st_size;
			}
			de.file_name = dp->d_name;
			cb(&de, data);
		}
		closedir(dirp);
	}
	return 1;
}

struct dir_scan_data {
	struct de *entries;
	int nentries;
 	int arr_size;
};

static void dir_scan_callback(struct de *de, void *data) {
	struct dir_scan_data *dsd = (struct dir_scan_data *)data;

	if (!dsd->entries || dsd->nentries >= dsd->arr_size) {
		dsd->arr_size *= 2;
		dsd->entries = (struct de *)realloc(dsd->entries,
							dsd->arr_size*sizeof(dsd->entries[0]));
	}
	if (!dsd->entries) {
		/* TODO(lsm): propagate an error to the caller */
		dsd->nentries = 0;
	}
	else {
		dsd->entries[dsd->nentries].file_name = strdup(de->file_name);
		dsd->entries[dsd->nentries].is_dir = de->is_dir;
		dsd->entries[dsd->nentries].mtime = de->mtime;
		dsd->entries[dsd->nentries].size = de->size;
		dsd->entries[dsd->nentries].conn = de->conn;
		dsd->nentries++;
	}
}

void handle_directory_request(struct http_connection *conn,
								 const char *dir) {
	int i, sort_direction;
	struct dir_scan_data data = { NULL, 0, 128 };
	char buf[64];

	if (!scan_directory(conn, dir, &data, dir_scan_callback)) {
		http_error(conn, "Cannot open directory \"%s\": %m", dir);
		http_send_response(conn, 500, "Cannot Open Directory \"%s\"", dir);
		return;
	}

	sort_direction = conn->request.query_string != NULL &&
		conn->request.query_string[1] == 'd' ? 'a' : 'd';

	conn->status_code = 200;
	conn->must_close = 1;
	if (conn->ctx->config->server_software)
		http_printf(conn, "HTTP/1.1 200 OK\r\n"
			"Server: %s\r\n"
			"Date: %s\r\n"
			"Connection: close\r\n"
			"Content-Type: text/html; charset=utf-8\r\n\r\n",
			conn->ctx->config->server_software,
			now(buf, sizeof(buf)));
	else
		http_printf(conn, "HTTP/1.1 200 OK\r\n"
			"Date: %s\r\n"
			"Connection: close\r\n"
			"Content-Type: text/html; charset=utf-8\r\n\r\n",
			now(buf, sizeof(buf)));

	conn->bytes_sent += http_printf(conn,
			"<html><head><title>Index of %s</title>"
			"<style>th {text-align: left;}</style></head>"
			"<body><h1>Index of %s</h1><pre><table cellpadding=\"0\">"
			"<tr><th><a href=\"?n%c\">Name</a></th>"
			"<th><a href=\"?d%c\">Modified</a></th>"
			"<th><a href=\"?s%c\">Size</a></th></tr>"
			"<tr><td colspan=\"3\"><hr></td></tr>",
			conn->request.uri, conn->request.uri,
			sort_direction, sort_direction, sort_direction);

	conn->bytes_sent += http_printf(conn,
			"<tr><td><a href=\"%s%s\">%s</a></td>"
			"<td>&nbsp;%s</td><td>&nbsp;&nbsp;%s</td></tr>\n",
			conn->request.uri, "..", "Parent directory", "-", "-");

	qsort(data.entries, (size_t) data.nentries, sizeof(data.entries[0]),
				compare_dir_entries);
	for (i = 0; i < data.nentries; i++) {
		print_dir_entry(&data.entries[i]);
		free(data.entries[i].file_name);
	}
	free(data.entries);

	conn->bytes_sent += http_printf(conn, "</table></body></html>");
}

static void print_props(struct http_connection *conn,
				const char* uri, int is_dir, time_t mtime, int64_t size)
{
	char modtime[64];

	gmt_date_string(modtime, sizeof(modtime), &mtime);
	conn->bytes_sent += http_printf(conn,
			"<d:response>"
			 "<d:href>%s</d:href>"
			 "<d:propstat>"
				"<d:prop>"
				 "<d:resourcetype>%s</d:resourcetype>"
				 "<d:getcontentlength>%" PRId64 "</d:getcontentlength>"
				 "<d:getlastmodified>%s</d:getlastmodified>"
				"</d:prop>"
				"<d:status>HTTP/1.1 200 OK</d:status>"
			 "</d:propstat>"
			"</d:response>\n",
			uri,
			is_dir ? "<d:collection/>" : "",
			size,
			modtime);
}

static void print_dav_dir_entry(struct de *de, void *data) {
	char href[PATH_MAX];
	struct http_connection *conn = (struct http_connection *) data;

	snprintf(href, sizeof(href), "%s%s",
				conn->request.uri, de->file_name);
	print_props(conn, href, de->is_dir, de->mtime, de->size);
}

void handle_propfind(struct http_connection *conn,
				const char *path, int is_dir, time_t mtime, int64_t size)
{
	const char *depth = http_get_header(conn, "Depth");
	char dbuf[64];

	conn->must_close = 1;
	conn->status_code = 207;
	if (conn->ctx->config->server_software)
		http_printf(conn, "HTTP/1.1 207 Multi-Status\r\n"
			"Server : %s\r\n"
			"Date : %s\r\n"
			"Connection: close\r\n"
			"Content-Type: text/xml; charset=utf-8\r\n\r\n",
			conn->ctx->config->server_software,
			now(dbuf, sizeof(dbuf)));
	else
		http_printf(conn, "HTTP/1.1 207 Multi-Status\r\n"
			"Date : %s\r\n"
			"Connection: close\r\n"
			"Content-Type: text/xml; charset=utf-8\r\n\r\n",
			now(dbuf, sizeof(dbuf)));

	conn->bytes_sent += http_printf(conn,
			"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
			"<d:multistatus xmlns:d='DAV:'>\n");

	/* Print properties for the requested resource itself */
	print_props(conn, conn->request.uri, is_dir, mtime, size);

	/* If it is a directory, print directory entries too if Depth is not 0 */
	if (is_dir && conn->ctx->config->allow_dirlist &&
			(depth == NULL || strcmp(depth, "0") != 0))
		scan_directory(conn, path, conn, &print_dav_dir_entry);
	conn->bytes_sent += http_printf(conn, "%s\n", "</d:multistatus>");
}

/*
 * For a given PUT path, create all intermediate subdirectories
 * for given path. Return 0 if the path itself is a directory,
 * or -1 on error, 1 if OK.
 */

int put_dir(struct http_connection *conn, const char *path) {
	char buf[PATH_MAX];
	const char *s, *p;
	struct stat st;
	int len, res = 1;

	for (s = p = path + 2; (p = strchr(s, '/')) != NULL; s = ++p) {
		len = p - path;
		if (len >= (int)sizeof(buf)) {
			res = -1;
			break;
		}
		memcpy(buf, path, len);
		buf[len] = '\0';

		/* Try to create intermediate directory */
		if (stat(buf, &st) < 0 && mkdir(buf, 0755) <= 0) {
			res = -1;
			break;
		}
		/* Is path itself a directory? */
		if (p[1] == '\0') {
			res = 0;
		}
	}
	return res;
}

void put_file(struct http_connection *conn, const char *path) {
	struct stat st;
	FILE *file;
	const char *range;
	int64_t r1, r2;
	int rc;
	char dbuf[64];

	conn->status_code = stat(path, &st) == 0 ? 200 : 201;

	if ((rc = put_dir(conn, path)) == 0) {
		if (conn->ctx->config->server_software)
			http_printf(conn, "HTTP/1.1 %d OK\r\n", 
					"Server: %d\r\n"
					"Date: %d\r\n\r\n",
				conn->status_code,
				conn->ctx->config->server_software,
				now(dbuf, sizeof(dbuf)));
		else
			http_printf(conn, "HTTP/1.1 %d OK\r\n", 
					"Date: %d\r\n\r\n",
				conn->status_code,
				now(dbuf, sizeof(dbuf)));
	}
	else if (rc == -1) {
		http_error(conn, "Cannot put_dir() \"%s\": %m", path);
		http_send_response(conn, 500, "Cannot Create Directory \"%s\"", path);
	}
	else if (!(file = fopen(path, "wb+"))) {
		http_error(conn, "Cannot open \"%s\": %m", path);
		http_send_response(conn, 500, "Cannot Open File \"%s\"", path);
	}
	else {
		range = http_get_header(conn, "Content-Range");
		r1 = r2 = 0;
		if (range != NULL && parse_range_header(range, &r1, &r2) > 0) {
			conn->status_code = 206;
			fseeko(file, r1, SEEK_SET);
		}
		if (http_forward_body_data(conn, file, -1, NULL)) {
			if (conn->ctx->config->server_software)
				http_printf(conn, "HTTP/1.1 %d OK\r\n", 
						"Server: %d\r\n"
						"Date: %d\r\n\r\n",
					conn->status_code,
					conn->ctx->config->server_software,
					now(dbuf, sizeof(dbuf)));
			else
				http_printf(conn, "HTTP/1.1 %d OK\r\n", 
						"Date: %d\r\n\r\n",
					conn->status_code,
					now(dbuf, sizeof(dbuf)));
			}
		fclose(file);
	}
}

int64_t http_get_content_len(struct http_connection *conn)
{
	return conn ? conn->content_len : -1;
}

int http_forward_body_data(struct http_connection *conn,
				FILE *fp, int sock, gnutls_session_t ssl)
{
	const char *expect, *body;
	char buf[BUFLEN];
	int to_read, nread, buffered_len, success = 0;

	expect = http_get_header(conn, "Expect");
	assert(fp != NULL);

	if (conn->content_len == -1)
		http_send_response(conn, 411, "Length Required");
	else if (expect != NULL && strcasecmp(expect, "100-continue"))
		http_send_response(conn, 417, "Expectation Failed");
	else {
		if (expect != NULL)
			http_printf(conn, "HTTP/1.1 100 Continue\r\n\r\n");

		body = conn->buf + conn->request_len + conn->consumed_content;
		buffered_len = &conn->buf[conn->data_len] - body;
		assert(buffered_len >= 0);
		assert(conn->consumed_content == 0);

		if (buffered_len > 0) {
			if ((int64_t) buffered_len > conn->content_len)
				buffered_len = (int) conn->content_len;
			push(fp, sock, ssl, body, (int64_t) buffered_len);
			conn->consumed_content += buffered_len;
		}

		nread = 0;
		while (conn->consumed_content < conn->content_len) {
			to_read = sizeof(buf);
			if ((int64_t) to_read > conn->content_len - conn->consumed_content)
				to_read = (int) (conn->content_len - conn->consumed_content);
			nread = pull(NULL, conn, buf, to_read);
			if (nread <= 0 || push(fp, sock, ssl, buf, nread) != nread)
				break;
			conn->consumed_content += nread;
		}

		if (conn->consumed_content == conn->content_len)
			success = nread >= 0;

		if (!success) {
			http_error(conn, "httpd_forward_body_data() failed");
			http_send_response(conn, 500, "Internal Server Error");
		}
	}
	return success;
}
