/*
 * Copyright (C) 2013, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <assert.h>
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <gcrypt.h>
#include <inttypes.h>
#include <limits.h>
#include <netdb.h>
#include <pthread.h>
#include <pwd.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <unistd.h>
#include "internals.h"

#if defined(USE_WEBSOCKET)

static int sha1_init(gcry_md_hd_t *gmh)
{
	return gcry_md_open(gmh, GCRY_MD_SHA1, 0) == 0;
}

static void sha1(gcry_md_hd_t gmh, const char *str)
{
	gcry_md_write(gmh, str, strlen(str));
}

static void sha1_char(gcry_md_hd_t gmh, char c)
{
	gcry_md_putc(gmh, c);
}

static int sha1_final(gcry_md_hd_t gmh, char *buf, size_t buflen)
{
	size_t i;
	char digest[20];
	uint32_t h[5];

	if (!buf || buflen < 41) 
		return 0;
	memcpy(digest, gcry_md_read(gmh, 0), 20);
	gcry_md_close(gmh);
	memcpy(h, digest, 20);
	for (i = 0; i < 5; i++) {
		char *p = (char *)&h[i];
		sprintf(buf+i*8, "%02x%02x%02x%02x", 
						*p&0xff, *(p+1)&0xff, *(p+2)&0xff, *(p+3)&0xff);
	}
	buf[40] = '\0';
	return 1;
}

static char *base64_encode(char *dst, const char *src, size_t size)
{
	static const char base64[] =
		/* RATS: ignore */
		"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	unsigned char in[3] = {0, 0, 0}; /* RATS: ignore */
	unsigned char out[4] = {0, 0, 0, 0}; /* RATS: ignore */
	int i, j, index, cols;
	size_t srclen, len;
	char *p, *plast;

	if (!src)
		return NULL;
	srclen = strlen(src);
	len = 4 * srclen /3 + (srclen % 3 ? 4 : 0) + 2 * ((srclen / 76) + 1);
	
	if (size < len)
		return NULL;

	for (plast = (char *)src; *plast; plast++);
	plast--;

	for (i = 0, j = 0, cols = 0, p = (char *)src; p <= plast; i++, p++) {
		index = i % 3;
		in[index] = *p;

		if (index == 2 || p == plast) {
			out[0] = ((in[0] & 0xfc) >> 2);
			out[1] = ((in[0] & 0x3) << 4) | ((in[1] & 0xf0) >> 4);
			out[2] = ((in[1] & 0xf) << 2) | ((in[2] & 0xc0) >> 6);
			out[3] = (in[2] & 0x3f);

			dst[j++] = base64[out[0]];
			dst[j++] = base64[out[1]];
			dst[j++] = index == 0 ? '=' : base64[out[2]];
			dst[j++] = index < 2 ? '=' : base64[out[3]];

			in[0] = in[1] = in[2] = 0;
			cols++;
		}
		if (cols == 19) {
			dst[j++] = '\r';
			dst[j++] = '\n';
			cols = 0;
		}
	}

	dst[j] = '\0';
	return dst;
}

static void send_websocket_handshake(struct http_connection *conn)
{
	static const char *magic = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
	char buf[100], sha[46], b64_sha[sizeof(sha) * 2];
	gcry_md_hd_t gmh;

	snprintf(buf, sizeof(buf), "%s%s",
				http_get_header(conn, "Sec-WebSocket-Key"), magic);
	sha1_init(&gmh);
	sha1(gmh, buf);
	sha1_final(gmh, sha, sizeof(sha));
	base64_encode(b64_sha, sha, sizeof(sha));
	if (conn->ctx->config->server_software)
		http_printf(conn, "HTTP/1.1 101 Switching Protocols\r\n"
			"Server: %s\r\nr"
			"Date: %s\r\n"
			"Upgrade: websocket\r\n"
			"Connection: upgrade\r\n"
			"Sec-WebSocket-Accept: %s\r\n\r\n",
			conn->ctx->config->server_software,
			now(buf, sizeof(buf)),
			b64_sha);
	else
		http_printf(conn, "HTTP/1.1 101 Switching Protocols\r\n"
			"Date: %s\r\n"
			"Upgrade: websocket\r\n"
			"Connection: upgrade\r\n"
			"Sec-WebSocket-Accept: %s\r\n\r\n",
			now(buf, sizeof(buf)),
			b64_sha);
}

static void read_websocket(struct http_connection *conn)
{
	unsigned char *buf = (unsigned char *)conn->buf+conn->request_len;
	int n;
	size_t i, len, mask_len, data_len, header_len, body_len;
	char mem[4 * 1024], *data;

	assert(conn->content_len == 0);
	for (;;) {
		header_len = 0;
		if ((body_len = conn->data_len - conn->request_len) >= 2) {
			len = buf[1] & 127;
			mask_len = buf[1] & 128 ? 4 : 0;
			if (len < 126 && body_len >= mask_len) {
				data_len = len;
				header_len = 2 + mask_len;
			}
			else if (len == 126 && body_len >= 4 + mask_len) {
				header_len = 4 + mask_len;
				data_len = ((((int) buf[2]) << 8) + buf[3]);
			}
			else if (body_len >= 10 + mask_len) {
				header_len = 10 + mask_len;
				data_len = (((uint64_t) htonl(* (uint32_t *) &buf[2])) << 32) +
					htonl(* (uint32_t *) &buf[6]);
			}
		}

		if (header_len > 0) { /* Allocate space to hold websocket payload */
			data = mem;
			if (data_len > sizeof(mem) && (data = malloc(data_len)) == NULL) {
				/*
				 * Allocation failed, exit the loop and then close 
				 * the connection 
				 * TODO: notify user about the failure
				 */
				break;
			}
			/* Read frame payload into the allocated buffer. */
			assert(body_len >= header_len);
			if (data_len + header_len > body_len) {
				len = body_len - header_len;
				memcpy(data, buf + header_len, len);
				/* TODO: handle pull error */
				pull(NULL, conn, data + len, data_len - len);
				conn->data_len = 0;
			}
			else {
				len = data_len + header_len;
				memcpy(data, buf + header_len, data_len);
				memmove(buf, buf + len, body_len - len);
				conn->data_len -= len;
			}
			/* Apply mask if necessary */
			if (mask_len > 0) {
				for (i = 0; i < data_len; i++) {
					data[i] ^= buf[header_len-mask_len+(i % 4)];
				}
			}
			/*
			 * Exit the loop if callback signalled to exit,
			 * or "connection close" opcode received.
			 */
			if ((conn->ctx->callbacks->websocket_data &&
					!conn->ctx->callbacks->websocket_data(conn,
											buf[0], data, data_len)) ||
					(buf[0] & 0xf) == 8) {	/* Opcode == 8, connection close */
				break;
			}
			if (data != mem) {
				free(data);
			}
			/* Not breaking the loop, process next websocket frame. */
		}
		else {
			/* Buffering websocket request */
			if ((n = pull(NULL, conn, conn->buf + conn->data_len,
					conn->buf_size - conn->data_len)) <= 0)
				break;
			conn->data_len += n;
		}
	}
}

void handle_websocket_request(struct http_connection *conn)
{
	if (strcmp(http_get_header(conn, "Sec-WebSocket-Version"), "13") != 0)
		http_send_response(conn, 400, "Bad Request\r\nSec-WebSocket-Version: 13");
	else if (conn->ctx->callbacks->websocket_connect &&
			 conn->ctx->callbacks->websocket_connect(conn) != 0)
		/* Callback has returned non-zero, do not proceed with handshake */;
	else {
		send_websocket_handshake(conn);
		if (conn->ctx->callbacks->websocket_ready)
			conn->ctx->callbacks->websocket_ready(conn);
		read_websocket(conn);
	}
}

int is_websocket_request(const struct http_connection *conn)
{
	const char *host, *upgrade, *connection, *version, *key;

	host = http_get_header(conn, "Host");
	upgrade = http_get_header(conn, "Upgrade");
	connection = http_get_header(conn, "Connection");
	key = http_get_header(conn, "Sec-WebSocket-Key");
	version = http_get_header(conn, "Sec-WebSocket-Version");

	return host != NULL && upgrade != NULL && connection != NULL &&
		key != NULL && version != NULL &&
		strcasestr(upgrade, "websocket") != NULL &&
		strcasestr(connection, "Upgrade") != NULL;
}
#endif // !USE_WEBSOCKET
