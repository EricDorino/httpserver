/*
 * Copyright (c) 2004-2012 Sergey Lyubka
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Copyright (C) 2013, 2016 Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef httpdserver_h_
#define httpdserver_h_

#include <gnutls/gnutls.h>
#include <sys/types.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

struct http_config {
	int debug, verbose;
	int daemonize;
	int nthreads;							/* # of concurrent threads */
	int nports;								/* TCP Ports to listen to */
	struct port {
		int port;								/* Port number, ie 80 or 443, or whatever */
		int local:1;						/* Listen on 127.0.0.1 instead of 0.0.0.0 */
		int ipv6:1;							/* Listen on IPv6 and IPv4 sockets */
		int is_ssl:1;						/* Enable TLS on this port */
		int ssl_redir:1;				/* Redirect to first TLS secure port */
	} ports[4];
	char *ssl_certificate;		/* PEM certificate, for tls connections */
	int keep_alive;						/* Try to keep connections alive if possible */
	int allow_update;					/* Allow update methods (PUT, DELETE, COPY, MOVE,
															 MKCOL) */
	int allow_dirlist;				/* Allow directory listing */
	int timeout;							/* Socket timeout */
	const char *docroot;			/* Document root */
	const char *user;					/* Unprivilegied user to run the server for */
	const char *index_files;	/* Comma separated list of possible index files */
	const char *hide_files;		/* Hide files pattern (regex) */
	const char *auth_type;		/* "basic" or "digest" */
	const char *server_software;	/* Server sofware */
	const char *server;				/* Server name */
	const char *domain;				/* Domain for auth/cookie handling purpose */
	const char *auth_program;	/* Auth program for basic authentication;
															 (/usr/sbin/pwauth protocol is used) */
	const char *auth_file;		/* Use this global password file for digest
															 authentication, rather than per-directory file */
	const char *protected_uri;/* URI's list that need authentication */
	const char *cgi_environ;	/* CGI list of comma separated name=value
															 user-defined variables */
	const char *cgi_pattern;	/* CGI script matching pattern (regex) */
	const char *ssi_pattern;	/* SSI matching pattern (regex) */
	int nrewrite_rules;				/* Rewrite rules /<regex>/<substitution>/ */
	const char **rewrite_rules;
};

struct http_context;
struct http_connection;

struct http_request {
  const char *method;
  const char *uri;
  const char *version;
  const char *query_string; /* URL part after '?', not including '?', or NULL */
  const char *remote_user;  /* Authenticated user, or NULL */
  int is_ssl;
  void *user_data;
  int nheaders;							/* HTTP headers */
  struct header {
    const char *name;
    const char *value;
  } headers[64];
};


struct http_callbacks {
	/*
	 * A request has been received:
	 *	return != 0: the request has been fullfilled by the callback.
	 *	return == 0: no data has been sent to the client.
	 */
  int (*begin_request)(struct http_connection *);

	/*
	 * A request has been fullfilled.
	 */
  void (*end_request)(const struct http_connection *, int reply_status_code);

	/*
	 * The server wants to log something.
	 * It is a good idea to specify such a callback, otherwise
	 * the server won't log anything.
	 */
  void (*log_message)(const struct http_connection *,
									int priority, const char *, va_list);

	/*
	 * A websocket request is received, before handshake takes place:
	 *	return != 0: the connection is closed immediately.
	 *	return == 0: the server proceed with handshake.
	 */
  int (*websocket_connect)(const struct http_connection *);

	/*
	 * A websocket is ready for data exchange
	 */
  void (*websocket_ready)(struct http_connection *);

	/*
	 * Data has been received from the client:
   *    bits: first byte of the websocket frame, see websocket RFC at
   *          http://tools.ietf.org/html/rfc6455, section 5.2
   *    data, datalen: payload, with mask (if any) already applied.
   *	return == 0: keep this websocket connection opened.
   *	return != 0: close this websocket connection.
	 */
  int  (*websocket_data)(struct http_connection *, int bits,
                         char *data, size_t datalen);

	/*
	 * The server has uploaded a file to a temporary location.
	 */
  void (*upload)(struct http_connection *, const char *file_name);

  /*
   * The server is about to send HTTP error to the client.
	 *	return != 0: the response has been sent by the callback.
	 *	return == 0: no data has been sent to the client.
   */
  int  (*http_response)(struct http_connection *,
									int status, const char *status_text, va_list);

	/*
	 * Callback specific gcrypt initialisation
	 */
	int (*local_gcry_init)(struct http_context *);
};

/*
 * Start the web server.
 */
extern struct http_context *
http_start(struct http_callbacks *, void *user_data, struct http_config *);

/*
 * Stop the web server.
 */
extern void http_stop(struct http_context *);

/*
 * Add, edit or delete the entry in the htpasswd file.
 * If password is NULL, entry is deleted.
 * Returns 1 on success, 0 on error.
 */
extern int
http_modify_passwords_file(const char *passwords_file_name,
                           const char *domain,
                           const char *user,
                           const char *password);


/*
 * Update connection informations from callback.
 */
extern void http_set_status_code(struct http_connection *, int code);
extern void http_must_close(struct http_connection *);
extern void http_add_bytes_sent(struct http_connection *, int bytes);

/*
 * Return the request for the current connection.
 */
extern struct http_request *http_get_request(struct http_connection *);

/*
 * Return the config for the current connection.
 */
extern struct http_config *http_get_config(struct http_connection *);

/*
 * Return the user specific data for the current connection.
 */
extern void *http_get_user_data(struct http_connection *);

/*
 * Return a fake connection. Used for initialisation.
 */
extern struct http_connection *http_fake_connection(struct http_context *);

/*
 * Errors and diagnostics output
 */
extern void http_error(struct http_connection *, const char *, ...);
extern void http_info(struct http_connection *, const char *, ...);
extern void http_trace(struct http_connection *, const char *, ...);

/*
 * Send data to the client.
 * Return:
 *  0   when the connection has been closed
 *  -1  on error
 *  >0  number of bytes written on success
 */
extern int http_write(struct http_connection *, const void *, size_t);
extern int http_printf(struct http_connection *, const char *, ...);
extern int http_vprintf(struct http_connection *, const char *, va_list);

#ifdef USE_WEBSOCKET

/*
 * Send data to a websocket client.
 * Return:
 *  0   when the connection has been closed
 *  -1  on error
 *  >0  number of bytes written on success
 */
extern int https_ws_write(struct http_connection *, const void *, size_t);
extern int https_ws_printf(struct http_connection *, const char *,...);
extern int http_ws_vprintf(struct http_connection *, const char *, va_list);

#endif

/*
 * Send an HTTP response to the client
 */
extern void http_send_response(struct http_connection *,
								int status, const char *, ...);


/*
 * Send contents of the entire file together with HTTP headers.
 */
extern void http_send_file(struct http_connection *conn, const char *file_path);


/*
 * Read data from the remote end, return number of bytes read.
 */
extern int http_read(struct http_connection *, void *, size_t);


/*
 * Forward data to real (CGI-like) handler.
 */
extern int http_forward_body_data(struct http_connection *,
								FILE *file, int fd, gnutls_session_t);

/*
 * Return the Content-length header value (-1 if none found)
 */
extern int64_t http_get_content_len(struct http_connection *);

/*
 * Keep reading the input into buf, increasing the number
 * of bytes read nread.
 */
extern int http_read_request(struct http_connection *, FILE *,
								char *buf, int, int *nread);

/*
 * Send at least len bytes from file at offset specified to the client.
 */
extern void http_send_file_data(struct http_connection *, FILE *file,
								int64_t, int64_t offset, int64_t len);

/*
 * Get the value of particular HTTP header, or NULL if it is not present.
 */
extern const char *
http_get_header(const struct http_connection *, const char *name);


/*
 * Parse headers in buf, storing them in the request.
 */
extern void http_parse_headers(char **buf, struct http_request *);


/*
 * Get a value of particular form variable.
 * Parameters:
 *   data: pointer to form-uri-encoded buffer.
 *				 This could be either POST data, or request.query_string.
 *   data_len: length of the encoded data.
 *   var_name: variable name to decode from the buffer
 *   dst: destination buffer for the decoded variable
 *   dst_len: length of the destination buffer
 * Return:
 *   On success, the length of the decoded variable.
 *   On error:
 *      -1: variable not found.
 *      -2: destination buffer is NULL, zero length or too small to hold the
 *          decoded variable.
 * The destination buffer is guaranteed to be zero-terminated if it is not
 * NULL or zero length.
 */
extern int http_get_var(const char *data, size_t data_len,
               const char *var_name, char *dst, size_t dst_len);


/*
 * Get a value of a cookie variable into the destination buffer.
 * The destination buffer is guaranteed to be zero-terminated. In case of
 * failure, dst[0] == '\0'. Note that RFC allows many occurrences of the same
 * parameter. This function returns only first occurrence.
 * Return:
 *   On success, the length of the value.
 *   On error:
 *      -1: either "Cookie:" header is not present at all or the requested
 *          parameter is not found.
 *      -2: destination buffer is NULL, zero length or too small to hold the
 *          value.
 */
extern int http_get_cookie(const char *cookie, const char *var_name,
                  char *buf, size_t buf_len);


/*
 * Download data from a remote web server.
 * Parameters:
 *   host: host name to connect to, e.g. "foo.com", or "10.12.40.1".
 *   port: port number, e.g. 80.
 *   use_ssl: wether to use SSL connection.
 *   error_buffer, error_buffer_size: error message placeholder.
 *   request_fmt,...: HTTP request.
 * Return:
 *   On success, valid pointer to the new connection,
 *      suitable for http_read().
 *   On error, NULL. and error_buffer contains error message.
 * Example:
 *   char ebuf[100];
 *   struct http_connection *conn;
 *   conn = http_download("google.com", 80, 0, ebuf, sizeof(ebuf),
 *                      "%s", "GET / HTTP/1.0\r\nHost: google.com\r\n\r\n");
 */
extern struct http_connection *
http_download(const char *host, int port, int use_ssl,
            char *error_buffer, size_t error_buffer_size,
            const char *request_fmt, ...);

/*
 * Close the connection opened by http_download().
 */
extern void http_close_connection(struct http_connection *);

/*
 * File upload service.
 * Each uploaded file gets saved into a temporary file and the
 * upload callback is called
 * Returns the number of uploaded files.
 */
extern int http_upload(struct http_connection *, const char *destination_dir);

#ifdef USE_WEBSOCKET

/*
 * Close a websocket
 */
extern void http_ws_close(struct http_connection *);

#endif

/*
 *	Create detached thread.
 *	Return == 0: success.
 *	Return != 0: error.
 */
typedef void * (*http_thread_func_t)(void *);
extern int http_start_thread(http_thread_func_t f, void *p);

/*
 *	Spawn an entire process
 *	Parameters:
 *		arpg: program arguments (argp[0] is program file);
 *  	envp: program environment
 *
 *	Return > 0: pid of spawned process.
 *	Return < 0: error.
 */
extern pid_t http_spawn_process(struct http_connection *,
								char * const argp[], char * const envp[],
								int fdin, int fdout, const char *dir);


/*
 * Return builtin mime type for the given file name.
 * For unrecognized extensions, "application/octet-stream" is returned.
 */
extern const char *http_get_mime_type(const char *file_name);


/*
 * Return the Basic auth password, if any supplied (the user will be stored
 * in the request.remote_user field).
 * Return NULL if not authenticated.
 * The returned string must be free()d by the caller.
 */
const char *http_basic_auth_password(struct http_connection *);


/*
 * Return server version.
 */
extern const char *http_version(void);
extern const char *http_tls_version(void);

#ifdef __cplusplus
}
#endif

#endif
