/*
 * Copyright (C) 2013, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <assert.h>
#include <ctype.h>
#include <inttypes.h>
#include <limits.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "internals.h"

#ifndef NO_CGI

#define CGI_ENV_SIZE	4096
#define MAX_CGI_ENV_VARS		64

struct cgi_env_block {
	char buf[CGI_ENV_SIZE];					/* Environment buffer */
	int len;												/* Space used */
	char *vars[MAX_CGI_ENV_VARS];		/* char **envp */
	int nvars;											/* Number of variables */
};

/*
 * Append VARIABLE=VALUE\0 string to the buffer, and add a respective 
 * pointer into the vars array.
 */

static char *add_var(struct http_connection *conn,
								struct cgi_env_block *block, const char *fmt, ...)
{
	int n, remaining_space;
	char *added;
	va_list ap;

	remaining_space = sizeof(block->buf) - block->len - 2;
	assert(remaining_space >= 0);

	added = block->buf + block->len;

	va_start(ap, fmt);
	n = vsnprintf(added, remaining_space, fmt, ap);
	va_end(ap);

	if (n > 0 &&
			n + 1 < remaining_space &&
			block->nvars < (int)ARRAY_SIZE(block->vars) - 2) {
		block->vars[block->nvars++] = added;
		block->len += n + 1;
	}
	else
		http_error(conn, "CGI env truncated at \"%s\"", fmt);
	return added;
}

static void prepare_cgi_environment(struct http_connection *conn,
							const char *prog, struct cgi_env_block *blk)
{
	const char *s, *slash;
	char *p, src_addr[20];
	int	i;
	struct sockaddr remote_sockaddr;
	socklen_t remote_len = sizeof(struct sockaddr);

	blk->len = blk->nvars = 0;

	add_var(conn, blk, "SERVER_NAME=%s", conn->ctx->config->server);
	add_var(conn, blk, "SERVER_ROOT=%s", conn->ctx->config->docroot);
	add_var(conn, blk, "DOCUMENT_ROOT=%s", conn->ctx->config->docroot);

	add_var(conn, blk, "GATEWAY_INTERFACE=CGI/1.1");
	add_var(conn, blk, "SERVER_PROTOCOL=HTTP/1.1");
	if (conn->ctx->config->server_software)
		add_var(conn, blk, "SERVER_SOFTWARE=%s",
										conn->ctx->config->server_software);
	else
		add_var(conn, blk, "SERVER_SOFTWARE=");
	add_var(conn, blk, "REDIRECT_STATUS=200"); /* For the sake of PHP */

	if (conn->socket.sockaddr.sa_family == AF_INET) {
		struct sockaddr_in sin;

		memcpy(&sin, &conn->socket.sockaddr, conn->socket.socklen);
		add_var(conn, blk, "SERVER_PORT=%d", ntohs(sin.sin_port));
	}
	else if (conn->socket.sockaddr.sa_family == AF_INET6) {
		struct sockaddr_in6 sin6;

		memcpy(&sin6, &conn->socket.sockaddr, conn->socket.socklen);
		add_var(conn, blk, "SERVER_PORT=%d", ntohs(sin6.sin6_port));
	}

	add_var(conn, blk, "REQUEST_METHOD=%s", conn->request.method);

	if (getpeername(conn->socket.fd, &remote_sockaddr, &remote_len) < 0) {
		add_var(conn, blk, "REMOTE_ADDR=");
		add_var(conn, blk, "REMOTE_PORT=0");
	}
	else {
		sockaddr_to_string(src_addr, sizeof(src_addr), &remote_sockaddr);
		add_var(conn, blk, "REMOTE_ADDR=%s", src_addr);
		if (remote_sockaddr.sa_family == AF_INET) {
			struct sockaddr_in sin;
	
			memcpy(&sin, &remote_sockaddr, remote_len);
			add_var(conn, blk, "REMOTE_PORT=%d", ntohs(sin.sin_port));
		}
		else if (conn->socket.sockaddr.sa_family == AF_INET6) {
			struct sockaddr_in6 sin6;
	
			memcpy(&sin6, &remote_sockaddr, remote_len);
			add_var(conn, blk, "REMOTE_PORT=%d", ntohs(sin6.sin6_port));
		}
	}

	add_var(conn, blk, "REQUEST_URI=%s", conn->request.uri);

	assert(conn->request.uri[0] == '/');
	slash = strrchr(conn->request.uri, '/');
	if ((s = strrchr(prog, '/')) == NULL)
		s = prog;
	add_var(conn, blk, "SCRIPT_NAME=%.*s%s", (int) (slash - conn->request.uri),
				 conn->request.uri, s);

	add_var(conn, blk, "SCRIPT_FILENAME=%s", prog);
	add_var(conn, blk, "PATH_TRANSLATED=%s", prog);
	add_var(conn, blk, "HTTPS=%s", conn->ssl == NULL ? "off" : "on");

	if ((s = http_get_header(conn, "Content-Type")))
		add_var(conn, blk, "CONTENT_TYPE=%s", s);

	if (conn->request.query_string)
		add_var(conn, blk, "QUERY_STRING=%s", conn->request.query_string);

	if ((s = http_get_header(conn, "Content-Length")))
		add_var(conn, blk, "CONTENT_LENGTH=%s", s);

	if ((s = getenv("PATH")))
		add_var(conn, blk, "PATH=%s", s);

	if (conn->path_info)
		add_var(conn, blk, "PATH_INFO=%s", conn->path_info);

	if ((s = getenv("LD_LIBRARY_PATH")))
		add_var(conn, blk, "LD_LIBRARY_PATH=%s", s);

	if ((s = getenv("PERLLIB")))
		add_var(conn, blk, "PERLLIB=%s", s);

	if (conn->request.remote_user) {
		add_var(conn, blk, "REMOTE_USER=%s", conn->request.remote_user);
		add_var(conn, blk, "AUTH_TYPE=%s", conn->ctx->config->auth_type);
	}

	for (i = 0; i < conn->request.nheaders; i++) {
		p = add_var(conn, blk, "HTTP_%s=%s",
				conn->request.headers[i].name,
				conn->request.headers[i].value);

		/* Convert variable name into uppercase, and change - to _ */
		for (; *p != '=' && *p != '\0'; p++) {
			if (*p == '-')
				*p = '_';
			*p = (char) toupper(* (unsigned char *) p);
		}
	}

	/* Add user-specified variables */
	if ((s = conn->ctx->config->cgi_environ))
		while ((p = strchrnul(s, ',')) && p != s) {
			add_var(conn, blk, "%.*s", p-s+1, s);
			if (*p == '\0')
				break;
			else s = p+1;
		}

	blk->vars[blk->nvars++] = NULL;
	blk->buf[blk->len++] = '\0';

	assert(blk->nvars < (int) ARRAY_SIZE(blk->vars));
	assert(blk->len > 0);
	assert(blk->len < (int) sizeof(blk->buf));
}

pid_t http_spawn_process(struct http_connection *conn,
							 char * const argp[], char * const envp[],
							 int fdin, int fdout, const char *dir)
{
	pid_t pid;

	if ((pid = fork()) < 0) {
		http_error(conn, "fork(): %m");
		return pid;
	}
	else if (pid == 0) {
		if (chdir(dir) < 0)
			http_error(conn, "chdir(%s): %m", dir);
		else if (dup2(fdin, 0) < 0)
			http_error(conn, "dup2(%d, 0): %m", fdin);
		else if (dup2(fdout, 1) < 0)
			http_error(conn, "dup2(%d, 1): %m", fdout);
		else {
			dup2(fdout, 2);
			close(fdin);
			close(fdout);
#if 0
			/* 
			 * After exec, all signal handlers are restored to their
			 * default values, with one exception of SIGCHLD. 
			 * According to POSIX.1-2001 and Linux's implementation, 
			 * SIGCHLD's handler will leave unchanged after exec
			 * if it was set to be ignored. Restore it to default action.
			 */
			signal(SIGCHLD, SIG_DFL);
#endif
			execve(argp[0], argp, envp);
			http_error(conn, "execve(%s): %m", argp[0]);
		}
		exit(EXIT_FAILURE);
	}
	close(fdin);
	close(fdout);
	return pid;
}

static const char *get_header(const struct http_request *ri, const char *name)
{
	int i;

	for (i = 0; i < ri->nheaders; i++)
		if (strcasecmp(name, ri->headers[i].name) == 0)
			return ri->headers[i].value;
	return NULL;
}

static void childreap(int sig) {
}

void handle_cgi_request(struct http_connection *conn, const char *prog)
{
	int headers_len, data_len, i, fdin[2], fdout[2];
	const char *status, *status_text;
	char buf[16384], *pbuf, dir[PATH_MAX], *p;
	struct http_request ri;
	struct cgi_env_block blk;
	char *args[2] = {NULL, NULL};
	FILE *in, *out;
	FILE *fout;
	pid_t pid;
	sighandler_t sigchld;

	prepare_cgi_environment(conn, prog, &blk);

	/* CGI must be executed in its own directory. 'dir' must point to the
	 * directory containing executable program, 'p' must point to the
	 * executable program name relative to 'dir'.
	 */
	snprintf(dir, sizeof(dir), "%s", prog);
	if ((p = strrchr(dir, '/')) != NULL)
		*p++ = '\0';
	else {
		dir[0] = '.', dir[1] = '\0';
		p = (char *)prog;
	}

	pid = (pid_t)-1;
	fdin[0] = fdin[1] = fdout[0] = fdout[1] = -1;
	in = out = NULL;

	sigchld = signal(SIGCHLD, childreap);

	if (pipe(fdin) != 0 || pipe(fdout) != 0) {
		http_error(conn, "Cannot create CGI pipe: %m");
		http_send_response(conn, 500, "CGI Processing Error");
		goto done;
	}

	args[0] = p;
	pid = http_spawn_process(conn, args, blk.vars, fdin[0], fdout[1], dir);
	fdin[0] = fdout[1] = -1; /*TODO*/

	if (pid < 0) {
		http_error(conn, "Cannot spawn CGI process [%s]: %m", prog);
		http_send_response(conn, 500, "CGI Processing Error");
		goto done;
	}

	if ((in = fdopen(fdin[1], "wb")) == NULL ||
			(out = fdopen(fdout[0], "rb")) == NULL) {
		http_error(conn, "Cannot open CGI descriptor: %m");
		http_send_response(conn, 500, "CGI Processing Error");
		goto done;
	}

	setbuf(in, NULL);
	setbuf(out, NULL);
	fout = out;

	/* Send POST data to the CGI process if needed */
	if (!strcmp(conn->request.method, "POST") &&
			!http_forward_body_data(conn, in, -1, NULL)) {
		goto done;
	}

	/* Close so child gets an EOF. */
	fclose(in);
	in = NULL;
	fdin[1] = -1;

	/*
	 * Now read CGI reply into a buffer. We need to set correct
	 * status code, thus we need to see all HTTP headers first.
	 * Do not send anything back to client, until we buffer in all
	 * HTTP headers.
	 */
	data_len = 0;
	headers_len = http_read_request(conn, out, buf, sizeof(buf), &data_len);
	if (headers_len <= 0) {
		http_error(conn, "CGI program sent malformed or too big (>%u bytes) "
						"HTTP headers: [%.*s]",
						(unsigned) sizeof(buf), data_len, buf);
		http_send_response(conn, 500, "CGI Processing Error");
		goto done;
	}

	if (waitpid(pid, NULL, 0) < 0) {
		http_error(conn, "Cannot waitpid() for CGI program: %m");
		http_send_response(conn, 500, "CGI Processing Error");
		goto done;
	}

	pbuf = buf;
	buf[headers_len - 1] = '\0';
	http_parse_headers(&pbuf, &ri);

	/* Make up and send the status line */
	status_text = "OK";
	if ((status = get_header(&ri, "Status")) != NULL) {
		conn->status_code = atoi(status);
		status_text = status;
		while (isdigit(*status_text) || *status_text == ' ')
			status_text++;
	}
	else if (get_header(&ri, "Location") != NULL)
		conn->status_code = 302;
	else
		conn->status_code = 200;

	if (get_header(&ri, "Connection") != NULL &&
			!strcasecmp(get_header(&ri, "Connection"), "keep-alive"))
		conn->must_close = 1;

	http_printf(conn, "HTTP/1.1 %d %s\r\n", conn->status_code, status_text);

	/* Send headers */
	for (i = 0; i < ri.nheaders; i++) {
		if (strcmp(ri.headers[i].name, "Status") != 0)
			http_printf(conn, "%s: %s\r\n",
					ri.headers[i].name, ri.headers[i].value);
	}
	http_write(conn, "\r\n", 2);

	/* Send chunk of data that may have been read after the headers */
	conn->bytes_sent += http_write(conn, buf + headers_len,
								 (size_t)(data_len - headers_len));

	/* Read the rest of CGI output and send to the client */
	http_send_file_data(conn, fout, 0, 0, INT64_MAX);

done:
	if (pid > 0)
		kill(pid, SIGKILL);
	signal(SIGCHLD, sigchld);
	if (fdin[0] != -1)
		close(fdin[0]);
	if (fdout[1] != -1)
		close(fdout[1]);

	if (in != NULL)
		fclose(in);
	else if (fdin[1] != -1)
		close(fdin[1]);

	if (out != NULL)
		fclose(out);
	else if (fdout[0] != -1)
		close(fdout[0]);
}
#endif /* !NO_CGI */
