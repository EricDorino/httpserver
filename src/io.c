/*
 * Copyright (C) 2013, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <inttypes.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include "internals.h"
#define BUFLEN 8192

/*
 * Print message to buffer. If buffer is large enough to hold the message,
 * return buffer. If buffer is to small, allocate large enough buffer on heap,
 * and return allocated buffer.
 */

static int alloc_vprintf(char **buf, size_t size, const char *fmt, va_list ap)
{
	va_list ap_copy;
	int len;

	va_copy(ap_copy, ap);
	len = vsnprintf(NULL, 0, fmt, ap_copy);

	if (len > (int)size &&
			(size = len + 1) > 0 &&
			(!(*buf = malloc(size))))
		len = -1;
	else {
		va_copy(ap_copy, ap);
		vsnprintf(*buf, size, fmt, ap_copy);
	}
	return len;
}

int http_vprintf(struct http_connection *conn, const char *fmt, va_list ap) {
	char mem[BUFLEN], *buf = mem;
	int len;

	if ((len = alloc_vprintf(&buf, sizeof(mem), fmt, ap)) > 0)
		len = http_write(conn, buf, (size_t) len);
	if (buf != mem && buf != NULL)
		free(buf);
	return len;
}

int http_printf(struct http_connection *conn, const char *fmt, ...) {
	va_list ap;
	int len;

	va_start(ap, fmt);
	len = http_vprintf(conn, fmt, ap);
	va_end(ap);
	return len;
}

/*
 * Write data to the IO channel - opened file descriptor, socket or TLS 
 * descriptor. Return number of bytes written.
 */

int64_t push(FILE *fp, int sock, gnutls_session_t ssl,
						 const char *buf, int64_t len)
{
	int64_t sent;
	int n, k;

	sent = 0;
	while (sent < len) {
		/* How many bytes we send in this iteration */
		k = len - sent > INT_MAX ? INT_MAX : (int)(len-sent);

		if (ssl)
			n = gnutls_write(ssl, buf + sent, k);
		else
			if (fp) {
				n = (int)fwrite(buf + sent, 1, (size_t)k, fp);
				if (ferror(fp))
					n = -1;
			}
			else
				n = send(sock, buf + sent, (size_t)k, MSG_NOSIGNAL);
		if (n <= 0)
			break;
		sent += n;
	}
	return sent;
}

/*
 * Read from IO channel - opened file descriptor, socket, or SSL descriptor.
 * Return negative value on error, or number of bytes read on success.
 */

int pull(FILE *fp, struct http_connection *conn, char *buf, int len)
{
	int nread;

	if (fp)
		/* Use read() instead of fread(), because if we're reading from the CGI
		 * pipe, fread() may block until IO buffer is filled up. We cannot afford
		 * to block and must pass all read bytes immediately to the client.
		 */
		nread = read(fileno(fp), buf, (size_t)len);
	else if (conn->ssl)
		nread = gnutls_read(conn->ssl, buf, len);
	else
		nread = recv(conn->socket.fd, buf, (size_t)len, 0);
	return conn->ctx->stop_flag ? -1 : nread;
}

int http_read(struct http_connection *conn, void *buf, size_t len)
{
	int n, buffered_len, nread;
	const char *body;

	nread = 0;
	if (conn->consumed_content < conn->content_len) {
		/* Adjust number of bytes to read.*/
		int64_t to_read = conn->content_len - conn->consumed_content;
		if (to_read < (int64_t) len)
			len = (size_t)to_read;

		/* Return buffered data */
		body = conn->buf + conn->request_len + conn->consumed_content;
		buffered_len = &conn->buf[conn->data_len] - body;
		if (buffered_len > 0) {
			if (len < (size_t) buffered_len)
				buffered_len = (int) len;
			memcpy(buf, body, (size_t) buffered_len);
			len -= buffered_len;
			conn->consumed_content += buffered_len;
			nread += buffered_len;
			buf = (char *) buf + buffered_len;
		}

		/*
		 * We have returned all buffered data.
		 * Read new data from the remote socket.
		 */
		while (len > 0) {
			n = pull(NULL, conn, (char *) buf, (int) len);
			if (n < 0) {
				nread = n;
				break;
			}
			else if (n == 0)
				break;	/* No more data to read */
			else {
				buf = (char *)buf + n;
				conn->consumed_content += n;
				nread += n;
				len -= n;
			}
		}
	}
	return nread;
}

int http_write(struct http_connection *conn, const void *buf, size_t len)
{
	return (int)push(NULL, conn->socket.fd, conn->ssl, (const char *) buf,
								 (int64_t) len);
}

