/*
 * Copyright (C) 2013, 2016 Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/stat.h>
#include <ctype.h>
#include <errno.h>
#include <gcrypt.h>
#include <getopt.h>
#include <limits.h>
#include <signal.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include "httpserver.h"

static int exit_flag = 0;
static struct http_context *ctx;

static void signal_handler(int sig)
{
  exit_flag = sig;
}

#define OPT	256
#define OPT_DOMAIN						(OPT+0)
#define OPT_DOCROOT						(OPT+1)
#define OPT_LOG_FACILITY			(OPT+2)
#define OPT_SSL_REDIRECT			(OPT+3)
#define OPT_ALLOW_DIRLIST			(OPT+4)
#define OPT_ALLOW_UPDATE			(OPT+5)
#define OPT_INDEX_FILES				(OPT+6)
#define OPT_AUTH_FILE					(OPT+7)
#define OPT_AUTH_PROGRAM			(OPT+8)
#define OPT_AUTH_TYPE					(OPT+9)
#define OPT_PROTECTED_URI			(OPT+10)
#define OPT_CGI_ENVIRON				(OPT+11)
#define OPT_CGI_PATTERN				(OPT+12)
#define OPT_SSI_PATTERN				(OPT+13)
#define OPT_REWRITE_RULE			(OPT+14)
#define OPT_SERVER						(OPT+15)

static struct option opt[] = {
	{"allow-directory-listing", no_argument, NULL, OPT_ALLOW_DIRLIST},
	{"allow-update", no_argument, NULL, OPT_ALLOW_UPDATE},
	{"auth_file", required_argument, NULL, OPT_AUTH_FILE},
	{"auth_program", required_argument, NULL, OPT_AUTH_PROGRAM},
	{"auth_type", required_argument, NULL, OPT_AUTH_TYPE},
	{"cgi-environ", required_argument, NULL, OPT_CGI_ENVIRON},
	{"cgi-pattern", required_argument, NULL, OPT_CGI_PATTERN},
	{"debug", no_argument, NULL, 'd'},
	{"document-root", required_argument, NULL, OPT_DOCROOT},
	{"domain", required_argument, NULL, OPT_DOMAIN},
	{"enable-keep-alive", no_argument, NULL, 'K'},
	{"help", no_argument, NULL, 'h'},
	{"index-files", required_argument, NULL, OPT_INDEX_FILES},
	{"local", no_argument, NULL, 'l'},
	{"log-facility", required_argument, NULL, OPT_LOG_FACILITY},
	{"port", required_argument, NULL, 'p'},
	{"protected-uri", required_argument, NULL, OPT_PROTECTED_URI},
	{"rewrite-rule", required_argument, NULL, OPT_REWRITE_RULE},
	{"server-name", required_argument, NULL, OPT_SERVER},
	{"show-server-software", no_argument, NULL, 'S'},
	{"ssi-pattern", required_argument, NULL, OPT_SSI_PATTERN},
	{"ssl-certificate", required_argument, NULL, 'C'},
	{"ssl-port", required_argument, NULL, 'P'},
	{"ssl-redirect", no_argument, NULL, OPT_SSL_REDIRECT},
	{"threads", required_argument, NULL, 'T'},
	{"timeout", required_argument, NULL, 't'},
	{"user", required_argument, NULL, 'u'},
	{"verbose", no_argument, NULL, 'v'},
	{"version", no_argument, NULL, 'V'},
	{NULL, 0, NULL, 0}
};

#define DEFAULT_DOCROOT				"."
#define DEFAULT_LOG_FACILITY	"local0"
#define DEFAULT_PORT					"8080"
#define DEFAULT_SSL_PORT			"8443"
#define DEFAULT_NTHREADS			"5"
#define DEFAULT_TIMEOUT				"30000"
#define DEFAULT_INDEX_FILES		"index.html,index.cgi"
#define DEFAULT_AUTH_TYPE			"basic"
#define DEFAULT_CGI_PATTERN		"^.*\\.cgi$"
#define DEFAULT_SSI_PATTERN		"^.*\\.shtml$"

#define INDEX_DOMAIN					9
#define INDEX_SERVER					18
#define INDEX_SERVER_SOFTWARE	19

static struct {
	const char *comment;
	const char *default_value;
} opt_opt[] = {
	{NULL, NULL},	
	{NULL, NULL},	
	{"use digest authentication global file,instead of per-directory htpasswd file", NULL},	
	{"basic authentication program", "/usr/sbin/pwauth"},	
	{"authentication type; basic and digest are supported", DEFAULT_AUTH_TYPE},	
	{"list of comma-separated name=value user-defined CGI variables", NULL},
	{"CGI scripts pattern (regex)", DEFAULT_CGI_PATTERN},
	{NULL, NULL},
	{"default directory", DEFAULT_DOCROOT},
	{"authentification and cookie domain", NULL},	/*INDEX_DOMAIN MUST BE UPDATED*/
	{NULL, NULL},
	{"show help and exit", NULL},
	{"comma-separated list of index files", DEFAULT_INDEX_FILES},
	{"accept only connections from localhost", NULL},
	{"syslog facility", DEFAULT_LOG_FACILITY},
	{"HTTP port", DEFAULT_PORT},
	{"authenticated URI (regex)", NULL},
	{"rewrite rule (/regex/substitution string/)", NULL},
	{"server name", NULL}, /* INDEX_SERVER MUST BE UPDATED! */
	{"show server software version", NULL}, /* INDEX_SERVER_SOFTWARE MUST BE UPDATED! */
	{"SSI (Server Side Include) file pattern (regex)", DEFAULT_SSI_PATTERN},
	{"SSL PEM certificate", NULL},
	{"secure HTTP port", DEFAULT_SSL_PORT},
	{"always redirect to SSL port", NULL},
	{"number of threads", DEFAULT_NTHREADS},
	{"request timeout (ms)", DEFAULT_TIMEOUT},
	{"user to run daemon", NULL},
	{NULL, NULL},
	{"show version and exit", NULL}
};

static void usage(void)
{
	size_t i;

  fputs("usage: httpd {options}*\n", stderr);
  fputs("       httpd --help\n", stderr);
  fputs("       httpd --version\n", stderr);
  fputs("options:\n", stderr);

  for (i = 0; opt[i].name; i++) {
    fprintf(stderr, "  %c%c%s--%s%c %s %c%s%c\n",
										opt[i].val < OPT?'-':'\0', 
										opt[i].val < OPT?opt[i].val:'\0', 
										opt[i].val < OPT?", ":"", 
										opt[i].name, 
										opt_opt[i].comment?':':'\0', 
										opt_opt[i].comment?opt_opt[i].comment:"", 
										opt_opt[i].default_value?'(':'\0',
										opt_opt[i].default_value?opt_opt[i].default_value:"",
										opt_opt[i].default_value?')':'\0');
  }
  exit(EXIT_FAILURE);
}

static void log_message(const struct http_connection *conn,
								int priority, const char *format, va_list args)
{
  vsyslog(priority, format, args);
}

#define MAX_REWRITE_RULES	5

int main(int argc, char **argv) {
	int c;
  struct http_callbacks callbacks;
	struct http_config config;
	int port = atoi(DEFAULT_PORT),
			ssl_port = atoi(DEFAULT_SSL_PORT),
			ssl_redir = 0,
			local = 0,
			facility = LOG_LOCAL0;
	int nrewrite_rules = 0;
	const char *rewrite_rules[MAX_REWRITE_RULES];
#if 0
	/* '~/<user>/...' -> '/home/<user>/public_html/...' */ 
			"/.*\\/~([a-zA-Z0-9_]*)(.*)/\\/home/\\1/public_html\\2"
#endif
	char server[255], domain[128], server_software[128];

  memset(&config, 0, sizeof(config));
	config.daemonize = config.debug ? 0 : 1;
	if (getdomainname(domain, sizeof(domain)) < 0 || 
			strcmp(domain, "(none)") == 0)
		strcpy(domain, "example.org");
	opt_opt[INDEX_DOMAIN].default_value = domain;	/* Beware */
	config.domain = domain;
	if (gethostname(server, sizeof(server)) < 0)
		strcpy(server, "unknown");
	if (!index(server, '.')) {
		strncat(server, ".", sizeof(server));
		strncat(server, domain, sizeof(server));
	}
	opt_opt[INDEX_SERVER].default_value = server;	/* Beware */
	config.server = server;
	config.auth_type = DEFAULT_AUTH_TYPE;
	config.nthreads = atoi(DEFAULT_NTHREADS);
	config.timeout = atoi(DEFAULT_TIMEOUT);
	config.docroot = DEFAULT_DOCROOT;
	config.user = NULL;
	config.hide_files = "^\\.|^~|.*~$";
	config.index_files = DEFAULT_INDEX_FILES;
	config.cgi_pattern = DEFAULT_CGI_PATTERN;
	config.ssi_pattern = DEFAULT_SSI_PATTERN;
	snprintf(server_software, sizeof(server_software), "%s/%s", PACKAGE, VERSION);
	opt_opt[INDEX_SERVER_SOFTWARE].default_value = server_software;	/* Beware */

  memset(&callbacks, 0, sizeof(callbacks));
  callbacks.log_message = &log_message;

	opterr = 0;
	for (;;) {
		int index = 0;

		if ((c = getopt_long(argc, argv, "C:dhKlP:p:ST:t:u:vV", opt, &index)) < 0)
			break;

		switch (c) {
			default:
				usage();
				break;
			case OPT_ALLOW_UPDATE:
				config.allow_update = 1;
				break;
			case OPT_ALLOW_DIRLIST:
				config.allow_dirlist = 1;
				break;
			case OPT_AUTH_FILE:
				config.auth_file = optarg;
				break;
			case OPT_AUTH_PROGRAM:
				config.auth_program = optarg;
				break;
			case OPT_AUTH_TYPE:
				config.auth_type = optarg;
				if (strcmp(config.auth_type, "basic") != 0 &&
						strcmp(config.auth_type, "digest") != 0)
						usage();
				break;
			case OPT_CGI_ENVIRON:
				config.cgi_environ = optarg;
				break;
			case OPT_CGI_PATTERN:
				config.cgi_pattern = optarg;
				break;
			case OPT_PROTECTED_URI:
				config.protected_uri = optarg;
				break;
			case OPT_SSI_PATTERN:
				config.ssi_pattern = optarg;
				break;
			case 'C':
				config.ssl_certificate = optarg;
				break;
			case OPT_INDEX_FILES:
				config.index_files = optarg;
				break;
			case 'K':
				config.keep_alive = 1;
				break;
			case 'l':
				local = 1;
				break;
			case 'p':
				port = atoi(optarg);
				break;
			case 'P':
				ssl_port = atoi(optarg);
				break;
			case OPT_SSL_REDIRECT:
				ssl_redir = 1;
				break;
			case 'T':
				config.nthreads = atoi(optarg);
				break;
			case 't':
				config.timeout = atoi(optarg);
				break;
			case 'u':
				config.user = optarg;
				break;
			case OPT_DOMAIN:
				config.domain = optarg;
				break;
			case OPT_DOCROOT:
				config.docroot = optarg;
				break;
			case OPT_LOG_FACILITY:
					if (strncmp(optarg, "local", 5) == 0 && strlen(optarg) == 6 && 
							optarg[5] >= '0' && optarg[5] <= '7')
						facility = LOG_LOCAL0 + atoi(optarg+5);
					else if (strcmp(optarg, "daemon") == 0)
						facility = LOG_DAEMON;
					else if (strcmp(optarg, "mail") == 0)
						facility = LOG_MAIL;
					else if (strcmp(optarg, "user") == 0)
						facility = LOG_USER;
					else usage();
				break;
			case OPT_REWRITE_RULE:
				if (nrewrite_rules >= MAX_REWRITE_RULES) {
					fputs("httpd: too many rewrite rules\n", stderr);
					return EXIT_FAILURE;
				}
				rewrite_rules[nrewrite_rules++] = optarg;
				break;
			case 'S':
				config.server_software = server_software;
				break;
			case 'v':
				config.verbose = 1;
				break;
			case 'd':
				config.debug = 1;
				config.verbose = 1;
				break;
			case 'h':
				usage();
				break;
			case 'V':
				fprintf(stderr, "httpd server, part of %s/%s\n\n",
					PACKAGE_NAME, PACKAGE_VERSION);
				fputs("Copyright (C) 2013,  Eric Dorino\n"
					"This is free software; see the source "
					"for copying conditions.  There is NO\n"
					"warranty; not even for MERCHANTABILITY "
					"of FITNESS FOR A PARTICULAR PURPOSE.\n",
					stderr);
				return EXIT_FAILURE;
		}
	}

	openlog("httpd", LOG_PID|LOG_PERROR, facility);
	if (config.debug)
		setlogmask(LOG_UPTO(LOG_DEBUG));
	else if (config.verbose)
		setlogmask(LOG_UPTO(LOG_INFO));
	else 
		setlogmask(LOG_UPTO(LOG_NOTICE));

  signal(SIGTERM, signal_handler);
  signal(SIGINT, signal_handler);

	config.nrewrite_rules = nrewrite_rules;
	config.rewrite_rules = rewrite_rules;

	config.nports = 0;
	if (port > 0) {
		config.ports[config.nports].port = port;
		config.ports[config.nports].local = local;
		config.ports[config.nports].ipv6 = 0; /* FIXME */
		config.ports[config.nports].is_ssl = 0;
		config.ports[config.nports++].ssl_redir = ssl_redir;
	}
	if (ssl_port > 0) {
		config.ports[config.nports].port = ssl_port;
		config.ports[config.nports].local = local;
		config.ports[config.nports].ipv6 = 0; /*FIXME */
		config.ports[config.nports].is_ssl = 1;
		config.ports[config.nports++].ssl_redir = 0;
	}

	if (config.nports == 0) {
    syslog(LOG_ERR, "no valid HTTP/HTTPS ports specified");
		exit(EXIT_FAILURE);
	}

  if (!(ctx = http_start(&callbacks, NULL, &config))) {
    syslog(LOG_ERR, "http_start() failed");
		exit(EXIT_FAILURE);
  }

  syslog(LOG_NOTICE, "httpd server started");

  while (exit_flag == 0)
    sleep(1);

  http_stop(ctx);

  syslog(LOG_NOTICE, "httpd server exited");

  return EXIT_SUCCESS;
}
