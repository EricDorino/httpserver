/*
 * Copyright (C) 2013, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef internals_h_
#define internals_h_

#include <arpa/inet.h>
#include <gnutls/gnutls.h>
#include <regex.h>
#include "httpserver.h"

#define MAX_REQUEST_SIZE	16384

/*
 * Describes listening socket, or socket which was accept()-ed by the master
 * thread and queued for future handling by the worker thread.
 */

struct socket {
	int fd;								/* Listening descriptor */
	struct sockaddr sockaddr;
	socklen_t socklen;
 	unsigned is_ssl:1;		/* Is port SSL-ed */
 	unsigned ssl_redir:1;	/* Is port supposed to redirect everything 
												 * to SSL port */
};

struct http_context {
	volatile int stop_flag;						/* Stop the event loop */

	struct http_config *config;				/* User defined config options */
	struct http_callbacks *callbacks;	/* User defined callback function */
	void *user_data;									/* User-defined data */

	gnutls_certificate_credentials_t cred; /* TLS/SSL certificate */

	struct socket *listening_sockets;
	int nlistening_sockets;

	regex_t cgi_pattern;							/* CGI path pattern, ie '.*\.cgi$' */
	regex_t ssi_pattern;							/* SSI path pattern, ie '.*\.shtml$' */
	regex_t hide_files;								/* Hide these files, ie '^.*\.' */
	regex_t protected_uri;						/* URI that requires authentification */
	regex_t *rewrite_rules;						/* Path rewrite */
	char **rewrite_rules_repl;				/* Substitution strings */

	volatile int nthreads;						/* Number of threads */
	pthread_mutex_t mutex;						/* Protects nthreads */
	pthread_cond_t cond;							/* Tracking workers terminations */

	struct socket queue[20];					/* Accepted sockets */
	volatile int sq_head;							/* Head of the socket queue */
	volatile int sq_tail;							/* Tail of the socket queue */
	pthread_cond_t sq_full;						/* Signaled when socket is produced */
	pthread_cond_t sq_empty;					/* Signaled when socket is consumed */
};

struct http_connection {
	struct http_request request;
	struct http_context *ctx;

	gnutls_session_t ssl;				/* TLS session */
	gnutls_priority_t prio;

	struct socket socket;				/* Socket to connected client */
	time_t birth_time;					/* Time when request was received */
	int64_t bytes_sent;					/* Total bytes sent to client */
	int64_t content_len;				/* Content-Length header value */
	int64_t consumed_content;		/* How many bytes of content have been read */
	char *buf;									/* Buffer for received data */
	char *path_info;						/* PATH_INFO part of the URL */
	int must_close;							/* 1 if connection must be closed */
	int buf_size;								/* Buffer size */
	int request_len;						/* Size of the request + headers in a buffer */
	int data_len;								/* Total size of data in a buffer */
	int status_code;						/* Last HTTP reply status code */
};

extern int should_keep_alive(const struct http_connection *);

extern char *skip_quoted(char **, const char *, const char *, char);
extern char *skip(char **, const char *);

extern int match(struct http_connection *, regex_t *, const char *);

extern void handle_file_request(struct http_connection *, const char *,
								time_t, int64_t);
extern void handle_propfind(struct http_connection *, const char *,
								int, time_t, int64_t);
extern void handle_directory_request(struct http_connection *, const char *);
extern int is_not_modified(const struct http_connection *, time_t, int64_t);
extern void send_file_data(struct http_connection *, FILE *, int64_t,
								int64_t, int64_t);
extern void put_file(struct http_connection *, const char *);
extern int put_dir(struct http_connection *, const char *);
extern int forward_body_data(struct http_connection *, FILE *, int, gnutls_session_t);

extern int is_websocket_request(const struct http_connection *);

extern void handle_ssi_file_request(struct http_connection *, const char *);
extern void handle_cgi_request(struct http_connection *, const char *);
extern void handle_websocket_request(struct http_connection *);

extern void send_digest_auth_request(struct http_connection *);
extern int check_digest_auth(struct http_connection *, const char *);

extern void send_basic_auth_request(struct http_connection *);
extern int check_basic_auth(struct http_connection *, const char *);

#define ARRAY_SIZE(a) (sizeof(a)/sizeof(a[0]))

extern int read_request(FILE *, struct http_connection *, char *, int, int *);
extern int get_request(struct http_connection *, char *, size_t);
extern void handle_request(struct http_connection *);
extern int get_request_len(const char *, int);
extern void parse_headers(char **, struct http_request *);

extern int64_t push(FILE *, int, gnutls_session_t, const char *, int64_t);
extern int pull(FILE *, struct http_connection *, char *, int);

extern void sockaddr_to_string(char *, size_t, const struct sockaddr *);

extern void close_connection(struct http_connection *);

extern char *url_encode(const char *, char *, size_t, const char *);
extern int url_decode(const char *, int, char *, int, int is_form_url_encoded);
extern void url_normalize(char *);

extern time_t parse_date_string(const char *);
extern int parse_date(const char *, time_t *, const time_t *);
extern void gmt_date_string(char *, size_t, time_t *);
extern char *now(char *, size_t);

extern void strlcpy(char *, const char *, size_t);
#endif
