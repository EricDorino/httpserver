/*
 * Copyright (C) 2018 Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef httpdserverxx_h_
#define httpdserverxx_h_
#include <stdexcept>
#include <httpserver.h>
#include <c_connector.h>

namespace http {

	class server {
		static server *_server;
		struct http_context *_ctx;
	public:
		static server *instance() {
			if (!_server)
				throw std::logic_error("no http::server::instance()");
			return _server;
		}

		server(void *userdata, struct http_config *cfg) {
			struct http_callbacks cb = {
				CB_begin_request,
				CB_end_request,
				CB_log_message,
				CB_websocket_connect,
				CB_websocket_ready,
				CB_websocket_data,
				CB_upload,
				CB_http_response,
				CB_local_gcry_init
			};
			_ctx = http_start(&cb, userdata, cfg);
		}

		virtual ~server() {
			if (_ctx)
				http_stop(_ctx);
		}
			
		void stop() {
			http_stop(_ctx);
			_ctx = nullptr;
		}

		/*
		 * Callbacks
		 */

		virtual int begin_request(http_connection *) = 0;
		virtual void end_request(const http_connection *, int) = 0;
		virtual void log_message(const http_connection *,
										int, const char *, va_list) = 0;
		virtual int websocket_connect(const http_connection *) = 0;
		virtual void websocket_ready(http_connection *) = 0;
		virtual int websocket_data(http_connection *, int, char *, size_t) = 0;
		virtual void upload(http_connection *, const char *) = 0;
		virtual int http_response(http_connection *, int, const char *, va_list) = 0;
		virtual int local_gcry_init(struct http_context *) = 0;

	};	

}

#endif
