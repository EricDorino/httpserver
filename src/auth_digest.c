/*
 * Copyright (C) 2013, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/stat.h>
#include <assert.h>
#include <ctype.h>
#include <gcrypt.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <unistd.h>
#include "internals.h"
#define BUFLEN 8192

/*
 * Digest authorization.
 */

static int md5_init(gcry_md_hd_t *gmh)
{
	return gcry_md_open(gmh, GCRY_MD_MD5, 0) == 0;
}

static void md5(gcry_md_hd_t gmh, const char *str)
{
	gcry_md_write(gmh, str, strlen(str));
}

static void md5_char(gcry_md_hd_t gmh, char c)
{
	gcry_md_putc(gmh, c);
}

static int md5_final(gcry_md_hd_t gmh, char *buf, size_t buflen)
{
	size_t i;
	char digest[16];
	uint32_t h[4];

	if (!buf || buflen < 33) 
		return 0;
	memcpy(digest, gcry_md_read(gmh, 0), 16);
	gcry_md_close(gmh);
	memcpy(h, digest, 16);
	for (i = 0; i < 4; i++) {
		char *p = (char *)&h[i];
		sprintf(buf+i*8, "%02x%02x%02x%02x", 
						*p&0xff, *(p+1)&0xff, *(p+2)&0xff, *(p+3)&0xff);
	}
	buf[32] = '\0';
	return 1;
}

static int check_password(const char *method, const char *ha1, const char *uri,
						const char *nonce, const char *nc, const char *cnonce,
						const char *qop, const char *response) {
	char ha2[33], expected_response[33];
	gcry_md_hd_t gmh;

	if (method == NULL || nonce == NULL || nc == NULL || cnonce == NULL ||
			qop == NULL || response == NULL)
		return 0;

	/*
	 * NOTE(lsm): due to a bug in MSIE, we do not compare the URI
	 * TODO(lsm): check for authentication timeout
	 */
#if 0
	if (strcmp(dig->uri, c->ouri) != 0 ||
			strlen(response) != 32 ||
			now - strtoul(dig->nonce, NULL, 10) > 3600)
#else
	if (strlen(response) != 32)
#endif
		return 0;

	if (!md5_init(&gmh))
		return 0;

	md5(gmh, method);
	md5_char(gmh, ':');
	md5(gmh, uri);
	md5_final(gmh, ha2, sizeof(ha2));

	if (!md5_init(&gmh))
		return 0;
	md5(gmh, ha1);
	md5_char(gmh, ':');
	md5(gmh, nonce);
	md5_char(gmh, ':');
	md5(gmh, nc);
	md5_char(gmh, ':');
	md5(gmh, cnonce);
	md5_char(gmh, ':');
	md5(gmh, qop);
	md5_char(gmh, ':');
	md5(gmh, ha2);
	md5_final(gmh, expected_response, sizeof(expected_response));

	return strcasecmp(response, expected_response) == 0;
}

/*
 *  Digest parsed authorization header
 */

struct auth_header {
	char *user, *uri, *cnonce, *response, *qop, *nc, *nonce;
};

static int parse_digest_auth_header(struct http_connection *conn,
				char *buf, size_t bufsize, struct auth_header *ah) {
	char *name, *value, *s;
	const char *auth_header;

	memset(ah, 0, sizeof(*ah));
	if ((auth_header = http_get_header(conn, "Authorization")) == NULL ||
			strncasecmp(auth_header, "Digest ", 7) != 0)
		return 0;

	/* Make modifiable copy of the auth header */
	strlcpy(buf, auth_header + 7, bufsize);
	s = buf;

	/* Parse authorization header */
	for (;;) {
		while (isspace(*s))
			s++;
		name = skip_quoted(&s, "=", " ", 0);
		/* Value is either quote-delimited, or ends at first comma or space.*/
		if (s[0] == '\"') {
			s++;
			value = skip_quoted(&s, "\"", " ", '\\');
			if (s[0] == ',')
				s++;
		}
		else
			value = skip_quoted(&s, ", ", " ", 0);	/* IE uses commas,
													   FF uses spaces */
		if (*name == '\0')
			break;

		if (!strcmp(name, "username"))
			ah->user = value;
		else if (!strcmp(name, "cnonce"))
			ah->cnonce = value;
		else if (!strcmp(name, "response"))
			ah->response = value;
		else if (!strcmp(name, "uri"))
			ah->uri = value;
		else if (!strcmp(name, "qop"))
			ah->qop = value;
		else if (!strcmp(name, "nc"))
			ah->nc = value;
		else if (!strcmp(name, "nonce"))
			ah->nonce = value;
	}
	/* CGI needs it as REMOTE_USER */
	if (ah->user != NULL)
		conn->request.remote_user = strdup(ah->user);
	else
		return 0;
	return 1;
}

/*
 * Use the global passwords file, if specified by auth_file option,
 * or search for .htpasswd in the requested directory.
 */

static FILE *auth_open(struct http_connection *conn, const char *path) {
	char name[PATH_MAX];
	struct stat st;
	const char *p, *e;

	if (conn->ctx->config->auth_file) { /* Use global passwords file */
		FILE *f;

		if (!(f = fopen(conn->ctx->config->auth_file, "r")))
			http_error(conn, "fopen(%s): %m", conn->ctx->config->auth_file);
		return f;
	}
	else if (stat(path, &st) == 0 && S_ISDIR(st.st_mode)) {
		snprintf(name, sizeof(name), "%s%s", path, "/.htpasswd");
		return fopen(name, "r");
	}
	else { /* Try to find .htpasswd in requested directory. */
		for (p = path, e = p + strlen(p) - 1; e > p; e--)
			if (e[0] == '/')
				break;
		snprintf(name, sizeof(name), "%.*s%s", (int)(e - p), p, "/.htpasswd");
		return fopen(name, "r");
	}
}

int check_digest_auth(struct http_connection *conn, const char *path)
{
	struct auth_header ah;
	char buf[BUFLEN], 
			 line[256], f_user[256], ha1[256], f_domain[256];
	FILE *authfile;
	int authorized = 1;

	if (!parse_digest_auth_header(conn, buf, sizeof(buf), &ah))
		return 0;

	if (!(authfile = auth_open(conn, path)))
		return 0;

	/* Loop over passwords file */
	while (fgets(line, sizeof(line), authfile)) {
		if (sscanf(line, "%[^:]:%[^:]:%s", f_user, f_domain, ha1) != 3)
			continue;
		if (strcmp(ah.user, f_user) == 0 &&
				strcmp(conn->ctx->config->domain, f_domain) == 0) {
			authorized = check_password(conn->request.method, ha1, ah.uri,
						ah.nonce, ah.nc, ah.cnonce, ah.qop, ah.response);
			break;
		}
	}
	fclose(authfile);
	return authorized;
}

void send_digest_auth_request(struct http_connection *conn)
{
	char buf[64];

	conn->status_code = 401;
	if (conn->ctx->config->server_software)
		http_printf(conn,
			"HTTP/1.1 401 Unauthorized\r\n"
			"Server: %s\r\n"
			"Date: %s\r\n"
			"WWW-Authenticate: Digest qop=\"auth\",realm=\"%s\",nonce=\"%lu\"\r\n\r\n",
			conn->ctx->config->server_software,
			now(buf, sizeof(buf)),
			conn->ctx->config->domain,
			(unsigned long)time(NULL));
	else
		http_printf(conn,
			"HTTP/1.1 401 Unauthorized\r\n"
			"Date: %s\r\n"
			"WWW-Authenticate: Digest qop=\"auth\",realm=\"%s\",nonce=\"%lu\"\r\n\r\n",
			now(buf, sizeof(buf)),
			conn->ctx->config->domain,
			(unsigned long)time(NULL));
}


/*
 * Modify the global or a local password file.
 */

int http_modify_passwords_file(const char *fname, const char *domain,
							 const char *user, const char *pass)
{
	gcry_md_hd_t gmh;
	int found;
	char line[512], u[512], d[512], ha1[33], tmp[PATH_MAX], buf[33];
	FILE *fp, *fp2;

	found = 0;
	fp = fp2 = NULL;

	/* Regard empty password as no password - remove user record. */
	if (pass && !*pass)
		pass = NULL;

	snprintf(tmp, sizeof(tmp), "%s.tmp", fname);

	/* Create the file if does not exist */
	if (!(fp = fopen(fname, "a+")))
		fclose(fp);

	/* Open the given file and temporary file */
	if (!(fp = fopen(fname, "r")))
		return 0;
	else if (!(fp2 = fopen(tmp, "w+"))) {
		fclose(fp);
		return 0;
	}

	/* Copy the stuff to temporary file */
	while (fgets(line, sizeof(line), fp) != NULL) {
		if (sscanf(line, "%[^:]:%[^:]:%*s", u, d) != 2)
			continue;

		if (!strcmp(u, user) && !strcmp(d, domain)) {
			found++;
			if (pass) {
				if (!md5_init(&gmh))
					return 0;
				md5(gmh, user);
				md5_char(gmh, ':');
				md5(gmh, domain);
				md5_char(gmh, ':');
				md5(gmh, pass);
				md5_final(gmh, ha1, sizeof(ha1));
				fprintf(fp2, "%s:%s:%s\n", user, domain, ha1);
			}
		}
		else
			fprintf(fp2, "%s", line);
	}

	/* If new user, just add it */
	if (!found && pass) {
		if (!md5_init(&gmh))
			return 0;
		md5(gmh, user);
		md5_char(gmh, ':');
		md5(gmh, domain);
		md5_char(gmh, ':');
		md5(gmh, pass);
		md5_final(gmh, ha1, sizeof(ha1));
		fprintf(fp2, "%s:%s:%s\n", user, domain, ha1);
	}

	fclose(fp);
	fclose(fp2);

	/* Put the temp file in place of real file */
	remove(fname);
	rename(tmp, fname);
	return 1;
}
