/*
 * Copyright (C) 2013, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "internals.h"
#define BUFLEN 8192


int http_upload(struct http_connection *conn, const char *destination_dir)
{
	const char *content_type_header, *boundary_start;
	char buf[BUFLEN], path[PATH_MAX], fname[1024], boundary[100], *s;
	FILE *fp;
	int bl, n, i, j, headers_len, boundary_len, len = 0, uploaded_files = 0;

	/* Request looks like this:
	 *
	 * POST /upload HTTP/1.1
	 * Host: 127.0.0.1:8080
	 * Content-Length: 244894
	 * Content-Type: multipart/form-data; boundary=----WebKitFormBoundaryRVr
	 *
	 * ------WebKitFormBoundaryRVr
	 * Content-Disposition: form-data; name="file"; filename="accum.png"
	 * Content-Type: image/png
	 *
	 * <89>PNG
	 * <PNG DATA>
	 * ------WebKitFormBoundaryRVr
	 */

	/* Extract boundary string from the Content-Type header */
	if ((content_type_header = http_get_header(conn, "Content-Type")) == NULL ||
		(boundary_start = strcasestr(content_type_header, "boundary=")) == NULL ||
			(sscanf(boundary_start, "boundary=\"%99[^\"]\"", boundary) == 0 &&
			 sscanf(boundary_start, "boundary=%99s", boundary) == 0) ||
			boundary[0] == '\0')
		return uploaded_files;

	boundary_len = strlen(boundary);
	bl = boundary_len + 4;	/* \r\n--<boundary> */
	for (;;) {
		/* Pull in headers */
		assert(len >= 0 && len <= (int) sizeof(buf));
		while ((n = http_read(conn, buf + len, sizeof(buf) - len)) > 0) 
			len += n;
		if ((headers_len = get_request_len(buf, len)) <= 0) 
			break;

		/* Fetch file name. */
		fname[0] = '\0';
		for (i = j = 0; i < headers_len; i++) {
			if (buf[i] == '\r' && buf[i + 1] == '\n') {
				buf[i] = buf[i + 1] = '\0';
				/*
				 * TODO(lsm): don't expect filename to be the 3rd field,
				 * parse the header properly instead.
				 */
				sscanf(&buf[j],
					"Content-Disposition: %*s %*s filename=\"%1023[^\"]",
					fname);
				j = i + 2;
			}
		}

		/* Give up if the headers are not what we expect */
		if (fname[0] == '\0')
			break;

		/* Move data to the beginning of the buffer */
		assert(len >= headers_len);
		memmove(buf, &buf[headers_len], len - headers_len);
		len -= headers_len;

		/*
		 * We open the file with exclusive lock held. This guarantee us
		 * there is no other thread can save into the same file simultaneously.
		 */
		fp = NULL;
		/* Construct destination file name. Do not allow paths to have slashes.*/
		if ((s = strrchr(fname, '/')) == NULL)
			s = fname;
		/* Open file in binary mode. TODO: set an exclusive lock. */
		snprintf(path, sizeof(path), "%s/%s", destination_dir, s);
		if ((fp = fopen(path, "wb")) == NULL)
			break;

		/* Read POST data, write into file until boundary is found. */
		n = 0;
		do {
			len += n;
			for (i = 0; i < len - bl; i++) {
				if (!memcmp(&buf[i], "\r\n--", 4) &&
						!memcmp(&buf[i + 4], boundary, boundary_len)) {
					/* Found boundary, that's the end of file data. */
					fwrite(buf, 1, i, fp);
					fflush(fp);
					uploaded_files++;
					if (conn->ctx->callbacks->upload != NULL)
						conn->ctx->callbacks->upload(conn, path);
					memmove(buf, &buf[i + bl], len - (i + bl));
					len -= i + bl;
					break;
				}
			}
			if (len > bl) {
				fwrite(buf, 1, len - bl, fp);
				memmove(buf, &buf[len - bl], bl);
				len = bl;
			}
		} while ((n = http_read(conn, buf + len, sizeof(buf) - len)) > 0);
		fclose(fp);
	}
	return uploaded_files;
}

