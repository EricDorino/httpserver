/*
 * Copyright (C) 2013, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <netinet/in.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <gcrypt.h>
#include <inttypes.h>
#include <netdb.h>
#include <pthread.h>
#include <pwd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <unistd.h>
#include "internals.h"

/*
 * logging and error reporting
 */

void sockaddr_to_string(char *buf, size_t buflen, const struct sockaddr *sa)
{
		*buf = '\0';
		if (sa->sa_family == AF_INET)
			inet_ntop(sa->sa_family, 
							(const void *)(&((struct sockaddr_in *)sa)->sin_addr),
							buf, buflen);
		else if (sa->sa_family == AF_INET6)
			inet_ntop(sa->sa_family, 
							(const void *)(&((struct sockaddr_in6 *)sa)->sin6_addr),
							buf, buflen);
}

static void vlog_message(struct http_connection *conn,
								int prio, const char *fmt, va_list args)
{
	if (conn->ctx->callbacks->log_message)
		conn->ctx->callbacks->log_message(conn, prio, fmt, args);
}

static void log_message(struct http_connection *conn,
								int prio, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	vlog_message(conn, prio, fmt, args);
	va_end(args);
}

void http_error(struct http_connection *conn, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	vlog_message(conn, LOG_ERR, fmt, args);
	va_end(args);
}

void http_info(struct http_connection *conn, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	vlog_message(conn, LOG_INFO, fmt, args);
	va_end(args);
}

void http_trace(struct http_connection *conn, const char *fmt, ...)
{
	va_list args;
	char buf[1024];

	va_start(args, fmt);
	snprintf(buf, sizeof(buf), "[%p] %s",
										(void *)pthread_self(), fmt);
	vlog_message(conn, LOG_DEBUG, buf, args);
	va_end(args);
}

static void log_access(struct http_connection *conn)
{
	if (conn->ctx->callbacks->log_message) {
		char date[64], src_addr[20];
		const char *referer, *user_agent;

		strftime(date, sizeof(date), "%d/%b/%Y %H:%M:%S %z",
					 localtime(&conn->birth_time));

		sockaddr_to_string(src_addr, sizeof(src_addr), &conn->socket.sockaddr);

		log_message(conn, LOG_NOTICE, 
				"%s %s [%s] \"%s %s %s%s\" %d %" PRId64 " %s %s",
				src_addr,
				conn->request.remote_user == NULL ? "-" : conn->request.remote_user,
				date,
				conn->request.method ? conn->request.method : "-",
				conn->request.uri ? conn->request.uri : "-",
				conn->request.version ? "HTTP/" : "",
				conn->request.version ? conn->request.version : "-",
				conn->status_code,
				conn->bytes_sent,
				(referer = http_get_header(conn, "Referer")) ? referer : "-",
				(user_agent = http_get_header(conn, "User-Agent")) ? user_agent : "-");
	}
}

/*
 * HTTP 1.1 assumes keep alive if "Connection:" header is not set. 
 * This function must tolerate situations when connection info is not
 * set up, for example if request parsing failed.
 */

int should_keep_alive(const struct http_connection *conn) {
	const char *version = conn->request.version;
	const char *header = http_get_header(conn, "Connection");

	return !(conn->must_close ||
					conn->status_code == 401 ||
					!conn->ctx->config->keep_alive ||
					(header && strcasecmp(header, "keep-alive") != 0) ||
					(!header && version && strcmp(version, "1.1") != 0));
}


void http_send_response(struct http_connection *conn,
						int status, const char *format, ...) {
	char buf[4096], dbuf[64];
	va_list ap;
	int len;

	conn->status_code = status;

	va_start(ap, format);

	if (!conn->ctx->callbacks->http_response ||
			!conn->ctx->callbacks->http_response(conn, status, format, ap)) {

		len = 5 + vsnprintf(buf, sizeof(buf), format, ap);

		http_trace(conn, "http_send_response %d %s", status, buf);

		if (status < 200 || status == 204 || status == 304) {
			/* Errors 1xx, 204 and 304 MUST NOT send a body */
			if (conn->ctx->config->server_software)
				http_printf(conn, "HTTP/1.1 %d %s\r\n"
					"Server: %s\r\n" 
					"Date: %s\r\n" 
					"Connection: %s\r\n\r\n", 
					status, buf,
					conn->ctx->config->server_software,
					now(dbuf, sizeof(dbuf)),
					should_keep_alive(conn) ?"keep-alive" : "close");
			else
				http_printf(conn, "HTTP/1.1 %d %s\r\n"
					"Date: %s\r\n" 
					"Connection: %s\r\n\r\n", 
					status, buf,
					now(dbuf, sizeof(dbuf)),
					should_keep_alive(conn) ?"keep-alive" : "close");
		}
		else {
			if (conn->ctx->config->server_software)
				http_printf(conn, "HTTP/1.1 %d %s\r\n"
					"Server: %s\r\n" 
					"Date: %s\r\n" 
					"Content-Length: %d\r\n"
					"Connection: %s\r\n\r\n", 
					status, buf,
					conn->ctx->config->server_software,
					now(dbuf, sizeof(dbuf)),
					len,
					should_keep_alive(conn) ?"keep-alive" : "close");
			else
				http_printf(conn, "HTTP/1.1 %d %s\r\n"
					"Date: %s\r\n" 
					"Content-Length: %d\r\n"
					"Connection: %s\r\n\r\n", 
					status, buf,
					now(dbuf, sizeof(dbuf)),
					len,
					should_keep_alive(conn) ?"keep-alive" : "close");
			conn->bytes_sent += http_printf(conn, "%d %s\n", status, buf);
		}
		va_end(ap);
	}
	else
		va_end(ap);
}

/*
 * Return fake connection structure. Used for logging, if connection
 * is not applicable at the moment of logging.
 */

static struct http_connection *fake_connection(struct http_context *ctx)
{
	static struct http_connection fake;

	fake.ctx = ctx;
	return &fake;
}

struct http_connection *http_fake_connection(struct http_context *ctx)
{
	return fake_connection(ctx);
}

/*
 * handle new connection
 */

static int is_valid_uri(const char *uri)
{
	return uri && (uri[0] == '/' || (uri[0] == '*' && uri[1] == '\0'));
}

static void process_connection(struct http_connection *conn)
{
	int keep_alive_enabled, keep_alive, discard_len;
	char ebuf[100];

	keep_alive_enabled = conn->ctx->config->keep_alive;
	keep_alive = 0;

	conn->data_len = 0;
	do {
		if (get_request(conn, ebuf, sizeof(ebuf)) < 0) {
			http_error(conn, "Cannot get_request(); ebuf: %s", ebuf);
			http_send_response(conn, 500, "Internal Server Error");
			conn->must_close = 1;
		}
		else if (!is_valid_uri(conn->request.uri)) {
			ebuf[0] = '*';
			http_send_response(conn, 400, "Bad Request URI \"%s\"",
											conn->request.uri);
		}
		else if (strcmp(conn->request.version, "1.0") &&
							 strcmp(conn->request.version, "1.1")) {
			ebuf[0] = '*';
			http_send_response(conn, 505, "HTTP Version Not Supported \"%s\"",
											conn->request.version);
		}

		if (ebuf[0] == '\0') {
			handle_request(conn);
			if (conn->ctx->callbacks->end_request)
				conn->ctx->callbacks->end_request(conn, conn->status_code);
		}
		log_access(conn);
		if (conn->request.remote_user != NULL) {
			free((void *)conn->request.remote_user);
			conn->request.remote_user = NULL;
		}

		/* 
		 * NOTE(lsm): order is important here. should_keep_alive() call
		 * is using parsed request, which will be invalid after memmove's below.
		 * Therefore, memorize should_keep_alive() result now for later use
		 * in loop exit condition.
		 */
		keep_alive = conn->ctx->stop_flag == 0 &&
					 keep_alive_enabled &&
					 conn->content_len >= 0 && 
					 should_keep_alive(conn);

		/*
		 * Discard all buffered data for this request
		 */
		discard_len = conn->content_len >= 0 &&
					  conn->request_len > 0 &&
			  conn->request_len + conn->content_len < (int64_t) conn->data_len ?
						(int) (conn->request_len+conn->content_len) : 
						conn->data_len;
		assert(discard_len >= 0);
		memmove(conn->buf, conn->buf+discard_len, conn->data_len-discard_len);
		conn->data_len -= discard_len;
		assert(conn->data_len >= 0);
		assert(conn->data_len <= conn->buf_size);
	} while (keep_alive);
}

/* Worker threads take accepted socket from the queue */
static int consume_socket(struct http_context *ctx, struct socket *sp)
{
	pthread_mutex_lock(&ctx->mutex);
	http_trace(fake_connection(ctx), "worker going idle");

	/* If the queue is empty, wait. We're idle at this point. */
	while (ctx->sq_head == ctx->sq_tail && ctx->stop_flag == 0)
		pthread_cond_wait(&ctx->sq_full, &ctx->mutex);

	/* If we're stopping, sq_head may be equal to sq_tail. */
	if (ctx->sq_head > ctx->sq_tail) {
		/* Copy socket from the queue and increment tail */
		*sp = ctx->queue[ctx->sq_tail % ARRAY_SIZE(ctx->queue)];
		ctx->sq_tail++;
		http_trace(fake_connection(ctx),
					"worker grabbed socket %d, going busy", sp->fd);

		/* Wrap pointers if needed */
		while (ctx->sq_tail > (int) ARRAY_SIZE(ctx->queue)) {
			ctx->sq_tail -= ARRAY_SIZE(ctx->queue);
			ctx->sq_head -= ARRAY_SIZE(ctx->queue);
		}
	}

	pthread_cond_signal(&ctx->sq_empty);
	pthread_mutex_unlock(&ctx->mutex);

	return !ctx->stop_flag;
}

static int open_tls_connection(struct http_connection *conn)
{
	gnutls_priority_t prio;
	int rc;

	gnutls_priority_init(&conn->prio, "NORMAL", NULL);

	if ((rc = gnutls_init(&conn->ssl, GNUTLS_SERVER)) < 0 ||
			(rc = gnutls_priority_set(conn->ssl, conn->prio)) < 0 ||
			(rc = gnutls_credentials_set(conn->ssl, 
										GNUTLS_CRD_CERTIFICATE, conn->ctx->cred)) < 0) {
		http_error(conn, "cannot initialize TLS session: %s", gnutls_strerror(rc));
		return 0;
	}
	gnutls_certificate_server_set_request(conn->ssl, GNUTLS_CERT_REQUEST);
	gnutls_session_enable_compatibility_mode(conn->ssl);
	gnutls_dh_set_prime_bits(conn->ssl, 1024);
	gnutls_transport_set_ptr(conn->ssl,
					(gnutls_transport_ptr_t)(long)conn->socket.fd);
	if ((rc = gnutls_handshake(conn->ssl)) < 0) {
		http_error(conn, "TLS handshake failed: %s", gnutls_strerror(rc));
		return 0;
	}
	return 1;
}

static void close_socket_gracefully(struct http_connection *conn)
{
	struct linger linger;
	int flags;

	/*
	 * Set linger option to avoid socket hanging out after close. This prevent
	 * ephemeral port exhaust problem under high QPS.
	 */
	linger.l_onoff = 1;
	linger.l_linger = 1;
	setsockopt(conn->socket.fd, SOL_SOCKET, SO_LINGER,
						 (char *) &linger, sizeof(linger));

	/* Send FIN to the client */
	shutdown(conn->socket.fd, SHUT_WR);
	flags = fcntl(conn->socket.fd, F_GETFL, 0);
	fcntl(conn->socket.fd, F_SETFL, flags|O_NONBLOCK);

	/* Now we know that our FIN is ACK-ed, safe to close */
	close(conn->socket.fd);
}

void close_connection(struct http_connection *conn)
{
	conn->must_close = 1;
	if (conn->ssl) {
		gnutls_bye(conn->ssl, GNUTLS_SHUT_RDWR);
		gnutls_priority_deinit(conn->prio);
		gnutls_deinit(conn->ssl);
		conn->ssl = NULL;
	}
	if (conn->socket.fd >= 0)
		close_socket_gracefully(conn);
}

static void *worker_thread(void *thread_func_param)
{
	struct http_context *ctx = thread_func_param;
	struct http_connection *conn;

	http_trace(fake_connection(ctx), "worker thread started");

	if (!(conn = calloc(1, sizeof(*conn)+MAX_REQUEST_SIZE)))
		http_error(fake_connection(ctx), "calloc() failed");
	else {
		conn->buf_size = MAX_REQUEST_SIZE;
		conn->buf = (char *)(conn+1);
		conn->ctx = ctx;
		conn->request.user_data = ctx->user_data;

		/*
		 * Call consume_socket() even when ctx->stop_flag > 0, to let it signal
		 * sq_empty condvar to wake up the master waiting in produce_socket()
		 */
		while (consume_socket(ctx, &conn->socket)) {
			conn->birth_time = time(NULL);
			conn->request.is_ssl = conn->socket.is_ssl;

			if (!conn->socket.is_ssl || open_tls_connection(conn))
				process_connection(conn);
			close_connection(conn);
		}
		free(conn);
	}

	/* Signal master that we're done with connection and exiting */
	pthread_mutex_lock(&ctx->mutex);
	ctx->nthreads--;
	pthread_cond_signal(&ctx->cond);
	assert(ctx->nthreads >= 0);
	pthread_mutex_unlock(&ctx->mutex);

	http_trace(fake_connection(ctx), "exiting");
	return NULL;
}

/*
 *  Master thread adds accepted socket to a queue
 */

static void produce_socket(struct http_context *ctx, const struct socket *sp)
{
	pthread_mutex_lock(&ctx->mutex);

	/* If the queue is full, wait */
	while (ctx->stop_flag == 0 &&
			 ctx->sq_head - ctx->sq_tail >= (int) ARRAY_SIZE(ctx->queue))
		pthread_cond_wait(&ctx->sq_empty, &ctx->mutex);

	if (ctx->sq_head - ctx->sq_tail < (int) ARRAY_SIZE(ctx->queue)) {
		/* Copy socket to the queue and increment head */
		ctx->queue[ctx->sq_head % ARRAY_SIZE(ctx->queue)] = *sp;
		ctx->sq_head++;
		http_trace(fake_connection(ctx), "queued socket %d", sp->fd);
	}

	pthread_cond_signal(&ctx->sq_full);
	pthread_mutex_unlock(&ctx->mutex);
}

static int set_sock_timeout(int fd, int milliseconds)
{
	struct timeval t;

	t.tv_sec = milliseconds / 1000;
	t.tv_usec = (milliseconds * 1000) % 1000000;
	return setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (void *) &t, sizeof(t)) ||
		setsockopt(fd, SOL_SOCKET, SO_SNDTIMEO, (void *) &t, sizeof(t));
}

static void accept_new_connection(const struct socket *listener,
								struct http_context *ctx)
{
	struct socket so;
	char src_addr[20];
	int on = 1;

	so.socklen = sizeof(struct sockaddr);

	if ((so.fd = accept(listener->fd, &so.sockaddr, &so.socklen)) < 0) {
		/* Nothing */
	}
	else {
		/* Put so socket structure into the queue */
		http_trace(fake_connection(ctx), "Accepted socket %d", (int) so.fd);
		so.is_ssl = listener->is_ssl;
		so.ssl_redir = listener->ssl_redir;
		getsockname(so.fd, &so.sockaddr, &so.socklen);
		setsockopt(so.fd, SOL_SOCKET, SO_KEEPALIVE, (void *)&on, sizeof(on));
		set_sock_timeout(so.fd, ctx->config->timeout);
		produce_socket(ctx, &so);
	}
}

static void close_all_listening_sockets(struct http_context *ctx)
{
	int i;

	for (i = 0; i < ctx->nlistening_sockets; i++)
		close(ctx->listening_sockets[i].fd);
	free(ctx->listening_sockets);
}

static void *master_thread(void *thread_func_param)
{
	struct http_context *ctx = thread_func_param;
	struct pollfd *pfd;
	int i;

	/* Increase priority of the master thread */
#if defined(ISSUE_317)
	struct sched_param sched_param;
	sched_param.sched_priority = sched_get_priority_max(SCHED_RR);
	pthread_setschedparam(pthread_self(), SCHED_RR, &sched_param);
#endif

	http_trace(fake_connection(ctx), "master thread starting");

	if (!(pfd = calloc(ctx->nlistening_sockets, sizeof(pfd[0])))) {
		http_error(fake_connection(ctx), "calloc() failed");
		ctx->stop_flag = 1;
	}
	while (ctx->stop_flag == 0) {
		for (i = 0; i < ctx->nlistening_sockets; i++) {
			pfd[i].fd = ctx->listening_sockets[i].fd;
			pfd[i].events = POLLIN;
		}
		if (poll(pfd, ctx->nlistening_sockets, 200) > 0)
			for (i = 0; i < ctx->nlistening_sockets; i++)
				if (ctx->stop_flag == 0 && (pfd[i].revents & POLLIN))
					accept_new_connection(&ctx->listening_sockets[i], ctx);
	}
	free(pfd);

	http_trace(fake_connection(ctx), "stopping workers");

	close_all_listening_sockets(ctx);

	/* Wakeup workers that are waiting for connections to handle. */
	pthread_cond_broadcast(&ctx->sq_full);

	/* Wait until all threads finish */
	pthread_mutex_lock(&ctx->mutex);
	while (ctx->nthreads > 0)
		pthread_cond_wait(&ctx->cond, &ctx->mutex);
	pthread_mutex_unlock(&ctx->mutex);

	/* All threads exited, no sync is needed. Destroy mutex and condvars */
	pthread_mutex_destroy(&ctx->mutex);
	pthread_cond_destroy(&ctx->cond);
	pthread_cond_destroy(&ctx->sq_empty);
	pthread_cond_destroy(&ctx->sq_full);

	http_trace(fake_connection(ctx), "exiting");

	ctx->stop_flag = 2;
	return NULL;
}

static void free_context(struct http_context *ctx)
{
	int i;

	regfree(&ctx->cgi_pattern);
	regfree(&ctx->ssi_pattern);
	regfree(&ctx->hide_files);
	regfree(&ctx->protected_uri);
	for (i = 0; i < ctx->config->nrewrite_rules; i++)
		regfree(&ctx->rewrite_rules[i]);
	free(ctx->rewrite_rules);
	free(ctx->rewrite_rules_repl);
	if (ctx->cred) 
		gnutls_certificate_free_credentials(ctx->cred);
	gnutls_global_deinit();
	free(ctx);
}

void http_stop(struct http_context *ctx)
{
	ctx->stop_flag = 1;

	while (ctx->stop_flag != 2)
		usleep(10000);
	free_context(ctx);
}


/*
 * Verify global auth file existence, if needed
 */

static int verify_auth_file(struct http_context *ctx)
{
	if (ctx->config->auth_file && access(ctx->config->auth_file, R_OK) < 0) {
		http_error(fake_connection(ctx), "cannot access %s: %m",
							ctx->config->auth_file);
		return 0;
	}
	return 1;
}

static int set_ssl_option(struct http_context *ctx)
{
	int i, size;
	int rc;
	char *pem;

	/* If PEM file is not specified, skip SSL initialization.*/
	if (!(pem = ctx->config->ssl_certificate))
		return 1;

	if ((rc = gnutls_global_init()) < 0 ||
			(rc = gnutls_certificate_allocate_credentials(&ctx->cred)) < 0 ||
			(rc = gnutls_certificate_set_x509_key_file(ctx->cred,
								pem, pem, GNUTLS_X509_FMT_PEM)) < 0) {
		http_error(fake_connection(ctx), "Cannot initialize TLS: %s",
										gnutls_strerror(rc));
		return 0;
	}

	http_trace(fake_connection(ctx), "tls layer initialized");
	return 1;
}

/*
 * Set uid/gid to specified user
 */

static int set_uid_option(struct http_context *ctx)
{
	struct passwd *pw;

	if (getuid() != 0 || !ctx->config->user)
		return 1;

	http_trace(fake_connection(ctx), "setting user %s", ctx->config->user);
	if (!(pw = getpwnam(ctx->config->user)))
		http_error(fake_connection(ctx), "Unknown user: %s", ctx->config->user);
	else if (setgid(pw->pw_gid) < 0 || setuid(pw->pw_uid) < 0)
		http_error(fake_connection(ctx),
						"Cannot set user %s :%m", ctx->config->user);
	else return 1;
	return 0;
}

static int set_port(struct socket *so,
								int port, int local, int ipv6,
								int is_ssl, int ssl_redir)
{
	struct sockaddr_in sin;
	struct sockaddr_in6 sin6;
	int on = 1, off = 0;

	if (!ipv6) {
		sin.sin_family = AF_INET;
		if (local)
			sin.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
		else
			sin.sin_addr.s_addr = htonl(INADDR_ANY);
		sin.sin_port = htons(port);
		memcpy(&so->sockaddr, &sin, so->socklen = sizeof(sin));
	}
	else {
		sin6.sin6_family = AF_INET6;
		if (local)
			sin6.sin6_addr = in6addr_loopback;
		else
			sin6.sin6_addr = in6addr_any;
		sin6.sin6_port = htons(port);
		memcpy(&so->sockaddr, &sin6, so->socklen = sizeof(sin6));
	}

	so->is_ssl = is_ssl;
	so->ssl_redir = ssl_redir;

	if ((so->fd = socket(so->sockaddr.sa_family, SOCK_STREAM, 0)) < 0 ||
			 setsockopt(so->fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) < 0 ||
			bind(so->fd, &so->sockaddr, so->socklen) < 0 ||
			listen(so->fd, SOMAXCONN) < 0) {
		int save_errno = errno;
		close(so->fd);
		errno = save_errno;
		return -1;
	}
	fcntl(so->fd, F_SETFD, FD_CLOEXEC);
	return 0;
}

static int set_ports_option(struct http_context *ctx)
{
	int i;

	if (ctx->config->nports <= 0) {
		http_error(fake_connection(ctx), "No listening port specified");
		return 0;
	}

	ctx->nlistening_sockets = ctx->config->nports;
	ctx->listening_sockets = malloc(
							ctx->nlistening_sockets*sizeof(ctx->listening_sockets[0]));
	if (!ctx->listening_sockets) {
		http_error(fake_connection(ctx), "Cannot alloc listening_sockets");
		return 0;
	}

	for (i = 0; i < ctx->config->nports; i++) {
		if (ctx->config->ports[i].is_ssl && !ctx->config->ssl_certificate) {
		  http_error(fake_connection(ctx), "No certificate for ssl socket");
			close_all_listening_sockets(ctx);
			return 0;
	}
		if (set_port(&ctx->listening_sockets[i],
									ctx->config->ports[i].port, 
									ctx->config->ports[i].local, 
									ctx->config->ports[i].ipv6, 
									ctx->config->ports[i].is_ssl, 
									ctx->config->ports[i].ssl_redir) < 0) {
		  http_error(fake_connection(ctx), "Cannot bind listening socket: %m");
			close_all_listening_sockets(ctx);
			return 0;
		}
		http_trace(fake_connection(ctx), "binding %s port %d (%s)", 
								ctx->config->ports[i].local ? "local" :"non local", 
								ctx->config->ports[i].port, 
								ctx->config->ports[i].ipv6 ? "ipv6" : "ipv4"); 

	}

	for (i = 0; i < ctx->nlistening_sockets; i++)
		http_trace(fake_connection(ctx), "listen on fd %d %s %s", 
								ctx->listening_sockets[i].fd,
								ctx->listening_sockets[i].is_ssl?"ssl ":"nossl",
								ctx->listening_sockets[i].ssl_redir?"ssl_redir":"nossl_redir");
	return 1;
}

int http_start_thread(http_thread_func_t func, void *param)
{
	pthread_t thread_id;
	pthread_attr_t attr;

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
#if 0
	pthread_attr_setstacksize(&attr, sizeof(struct http_connection) * 5);
#endif
	return pthread_create(&thread_id, &attr, func, param);
}

static int compile_pattern(struct http_context *ctx,
								regex_t *preg, const char *regex)
{
	int code;
	char err[255];

	if ((code = regcomp(preg, regex, REG_EXTENDED|REG_NOSUB|REG_NEWLINE)) != 0) {
		regerror(code, preg, err, sizeof(err));
		http_error(fake_connection(ctx) , "Cannot compile regex \"%s\": %s",
										regex, err);
		return 0;
	}
	return 1;
}

/*
 * Rewrite rules.
 * rule is "/<regex>/<repl>"
 * where
 * - '/' can be replaced by any char, except special regex chars,
 *   provided that it is replaced anywhere (i.e "*<regex>*<repl>").
 * - <repl> is a string where '&' means the original string, '\1' thru '\9'
 *   the substring matches.
 */

static int compile_rewrite_rule(struct http_context *ctx,
								regex_t *preg, char **repl, const char *rule)
{
	char sep, *p, *regex;
	int code;
	char err[255];
	
	if (!rule || strlen(rule) < 3)
		return 0;

	sep = rule[0];
	p = (char *)rule+1;
	while (*p && *p != sep)
		if (*p == '\\')
			p += 2;
		else
			p++;
	if (*p != sep) {
		http_error(fake_connection(ctx), "mangled rewrite rule \"%s\"", rule);
		return 0;
	}
	if (!(regex = strndup(&rule[1], p-&rule[1]))) {
		http_error(fake_connection(ctx), "strndup() failed");
		return 0;
	}
	p++;
	*repl = p;

	http_trace(fake_connection(ctx), "rewrite rule \"%s\" -> \"%s\"",
									regex, *repl);

	if ((code = regcomp(preg, regex, REG_EXTENDED|REG_NEWLINE)) != 0) {
		regerror(code, preg, err, sizeof(err));
		http_error(fake_connection(ctx) , "Cannot compile regex \"%s\": %s",
										regex, err);
		return 0;
	}
	free(regex);
	return 1;
}

static int compile_rewrite_rules(struct http_context *ctx)
{
	int i;

	if (!(ctx->rewrite_rules =
						calloc(ctx->config->nrewrite_rules, sizeof(regex_t)))) {
		http_error(fake_connection(ctx), "calloc() failed");
		return 0;
	}
	if (!(ctx->rewrite_rules_repl =
						calloc(ctx->config->nrewrite_rules, sizeof(char *)))) {
		http_error(fake_connection(ctx), "calloc() failed");
		return 0;
	}

	for (i = 0; i < ctx->config->nrewrite_rules; i++) {
		if (!compile_rewrite_rule(ctx,
							&ctx->rewrite_rules[i], 
							&ctx->rewrite_rules_repl[i], 
							ctx->config->rewrite_rules[i]))
			return 0;
		http_trace(fake_connection(ctx), "rewrite rule repl %d %s",
									i, ctx->rewrite_rules_repl[i]);
		}

	return 1;
}

GCRY_THREAD_OPTION_PTHREAD_IMPL;

struct http_context *http_start(struct http_callbacks *callbacks,
							void *user_data, struct http_config *config)
{
	struct http_context *ctx;
	const char *name, *value, *default_value;
	int i;

	if (!(ctx = calloc(1, sizeof(*ctx)))) {
		http_error(fake_connection(ctx), "calloc() failed");
		return NULL;
	}

	ctx->callbacks = callbacks;
	ctx->user_data = user_data;
	ctx->config = config;

	if (ctx->config->nthreads <= 0)
		ctx->config->nthreads = 2;
	if (ctx->config->timeout <= 100)
		ctx->config->timeout = 30000;

	if (strcasecmp(ctx->config->auth_type, "digest") != 0) {
		ctx->config->auth_type = "basic";
		if (!ctx->config->auth_program)
			ctx->config->auth_program = "/usr/sbin/pwauth";
	}

	if (ctx->config->cgi_pattern && 
			!compile_pattern(ctx, &ctx->cgi_pattern, ctx->config->cgi_pattern)) {
		free_context(ctx);
		return NULL;
	}
	if (ctx->config->ssi_pattern && 
			!compile_pattern(ctx, &ctx->ssi_pattern, ctx->config->ssi_pattern)) {
		free_context(ctx);
		return NULL;
	}
	if (ctx->config->hide_files && 
			!compile_pattern(ctx, &ctx->hide_files, ctx->config->hide_files)) {
		free_context(ctx);
		return NULL;
	}
	if (ctx->config->protected_uri && 
			!compile_pattern(ctx, &ctx->protected_uri, ctx->config->protected_uri)) {
		free_context(ctx);
		return NULL;
	}
	if (ctx->config->nrewrite_rules > 0 && 
			!compile_rewrite_rules(ctx)) {
		free_context(ctx);
		return NULL;
	}

	if (!verify_auth_file(ctx) ||
			!set_ssl_option(ctx) ||
			!set_ports_option(ctx) ||
			!set_uid_option(ctx)) {
		free_context(ctx);
		return NULL;
	}

	gcry_control(GCRYCTL_SET_THREAD_CBS, &gcry_threads_pthread);
	if (!gcry_check_version(GCRYPT_VERSION)) {
		http_error(fake_connection(ctx), "Cannot initialize gcrypt");
		free_context(ctx);
		return NULL;
	}
	gcry_control(GCRYCTL_DISABLE_SECMEM_WARN, 0);
	gcry_control(GCRYCTL_ENABLE_QUICK_RANDOM, 0);
	if (ctx->callbacks->local_gcry_init)
		ctx->callbacks->local_gcry_init(ctx);
	else 
		gcry_control(GCRYCTL_INITIALIZATION_FINISHED, 0);

	if (!ctx->config->debug && ctx->config->daemonize && daemon(0, 0) < 0) {
		http_error(fake_connection(ctx), "Cannot daemonize: %m");
		free_context(ctx);
		return NULL;
	}

	signal(SIGPIPE, SIG_IGN); /* Do not abort is remote disconnect */

	pthread_mutex_init(&ctx->mutex, NULL);
	pthread_cond_init(&ctx->cond, NULL);
	pthread_cond_init(&ctx->sq_empty, NULL);
	pthread_cond_init(&ctx->sq_full, NULL);

	http_start_thread(master_thread, ctx);

	for (i = 0; i < ctx->config->nthreads; i++)
		if (http_start_thread(worker_thread, ctx) != 0)
			http_error(fake_connection(ctx), "Cannot start worker thread: %m");
		else
			ctx->nthreads++;

	http_trace(fake_connection(ctx), "Started %d worker threads", ctx->nthreads);

	return ctx;
}

const char *http_version(void)
{
	return VERSION;
}

const char *http_tls_version(void)
{
	return gnutls_check_version(NULL);
}
