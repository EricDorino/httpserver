/*
 * Copyright (C) 2013, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <assert.h>
#include <ctype.h>
#include <inttypes.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include "internals.h"

void http_parse_headers(char **buf, struct http_request *req) {
	int i;

	for (i = 0; i < (int) ARRAY_SIZE(req->headers); i++) {
		req->headers[i].name = skip_quoted(buf, ":", " ", 0);
		req->headers[i].value = skip(buf, "\r\n");
		if (req->headers[i].name[0] == '\0')
			break;
		req->nheaders = i + 1;
	}
}

static int is_valid_http_method(const char *method)
{
	return !strcmp(method, "GET") || !strcmp(method, "POST") ||
		!strcmp(method, "HEAD") || !strcmp(method, "CONNECT") ||
		!strcmp(method, "PUT") || !strcmp(method, "DELETE") ||
		!strcmp(method, "OPTIONS") || !strcmp(method, "PROPFIND") ||
		!strcmp(method, "PROPPATCH") || !strcmp(method, "MKCOL") ||
		!strcmp(method, "COPY") || !strcmp(method, "MOVE") ||
		!strcmp(method, "LOCK") || !strcmp(method, "UNLOCK") ||
		!strcmp(method, "TRACE");
}

static int is_acceptable_http_method(const char *method)
{
	return !strcmp(method, "GET") || !strcmp(method, "POST") ||
		!strcmp(method, "HEAD") || !strcmp(method, "PUT") ||
		!strcmp(method, "DELETE") || !strcmp(method, "OPTIONS") ||
		!strcmp(method, "PROPFIND");
}

/*
 * Check whether full request is buffered. Return:
 * 		-1  if request is malformed
 * 		0  if request is not yet fully buffered
 * 		>0  actual request length, including last \r\n\r\n
 */

int get_request_len(const char *buf, int buflen)
{
	const char *s, *e;
	int len = 0;

	for (s = buf, e = s + buflen - 1; len <= 0 && s < e; s++) {
		/* Control characters are not allowed but >=128 is. */
		if (!isprint(*s) && *s != '\r' &&
				*s != '\n' && *(unsigned char *)s < 128) {
			len = -1;
			break;	/* [i_a] abort scan as soon as one malformed character
					 * is found; 
					 * don't let subsequent \r\n\r\n win us over anyhow
					 */
		}
		else if (s[0] == '\n' && s[1] == '\n')
			len = (int) (s - buf) + 2;
		else if (s[0] == '\n' && &s[1] < e && s[1] == '\r' && s[2] == '\n')
			len = (int) (s - buf) + 3;
	}
	return len;
}

static int parse_http_message(char *buf, int len, struct http_request *req)
{
	int is_request, request_len = get_request_len(buf, len);

	if (request_len > 0) {
		/* 
		 * Reset attributes.
		 * DO NOT TOUCH is_ssl 
		 */
		req->remote_user = req->method = req->uri = req->version = NULL;
		req->nheaders = 0;

		buf[request_len - 1] = '\0';

		/* RFC says that all initial whitespaces should be ingored */
		while (*buf != '\0' && isspace(*buf))
			buf++;
		req->method = skip(&buf, " ");
		req->uri = skip(&buf, " ");
		req->version = skip(&buf, "\r\n");
		if (((is_request = is_valid_http_method(req->method)) &&
				 memcmp(req->version, "HTTP/", 5) != 0) ||
				(!is_request && memcmp(req->method, "HTTP/", 5)) != 0)
			request_len = -1;
		else {
			if (is_request)
				req->version += 5;
			http_parse_headers(&buf, req);
		}
	}
	return request_len;
}

/*
 * Keep reading the input (either opened file descriptor fd, or socket sock,
 * or SSL descriptor ssl) into buffer buf, until \r\n\r\n appears in the
 * buffer (which marks the end of HTTP request). Buffer buf may already
 * have some data. The length of the data is stored in nread.
 * Upon every read operation, increase nread by the number of bytes read.
 */

int http_read_request(struct http_connection *conn, FILE *fp,
						char *buf, int bufsiz, int *nread)
{
	int request_len, n = 0;

	request_len = get_request_len(buf, *nread);
	while (*nread < bufsiz &&
				request_len == 0 &&
				 (n = pull(fp, conn, buf + *nread, bufsiz - *nread)) > 0) {
		*nread += n;
		assert(*nread <= bufsiz);
		request_len = get_request_len(buf, *nread);
	}
	return request_len <= 0 && n <= 0 ? -1 : request_len;
}

/*
 *  Return HTTP header value, or NULL if not found.
 */

const char *
http_get_header(const struct http_connection *conn, const char *name) {
	int i;

	for (i = 0; i < conn->request.nheaders; i++)
		if (strcasecmp(name, conn->request.headers[i].name) == 0)
			return conn->request.headers[i].value;
	return NULL;
}


int get_request(struct http_connection *conn, char *buf, size_t buflen)
{
	const char *cl;

	buf[0] = '\0';
	conn->path_info = NULL;
	conn->bytes_sent = conn->consumed_content = 0;
	conn->status_code = -1;
	conn->must_close = 0;
	conn->request_len = http_read_request(conn, NULL, conn->buf, conn->buf_size,
												 &conn->data_len);
	assert(conn->request_len < 0 || conn->data_len >= conn->request_len);

	if (conn->request_len == 0 && conn->data_len == conn->buf_size)
		strncpy(buf, "Request Too Large", buflen);
	else if (conn->request_len <= 0)
		strncpy(buf, "Client Closed Connection", buflen);
	else if (parse_http_message(conn->buf, conn->buf_size,
											&conn->request) <= 0)
		snprintf(buf, buflen, "Bad Request: [%.*s]", conn->data_len, conn->buf);
	else {
		if ((cl = http_get_header(conn, "Content-Length")))
			conn->content_len = strtoll(cl, NULL, 10);
		else if (!strcasecmp(conn->request.method, "POST") ||
					 !strcasecmp(conn->request.method, "PUT"))
			conn->content_len = -1;
		else
			conn->content_len = 0;
		conn->birth_time = time(NULL);
	}
	return buf[0] == '\0';
}

static int is_put_or_delete_request(const struct http_connection *conn) {
	const char *s = conn->request.method;

	return s != NULL && (strcmp(s, "PUT") == 0 || strcmp(s, "DELETE") == 0);
}

static int get_first_ssl_listener_index(const struct http_context *ctx) {
	int i, index = -1;

	for (i = 0; index == -1 && i < ctx->nlistening_sockets; i++) {
		index = ctx->listening_sockets[i].is_ssl ? i : -1;
	}
	return index;
}

static void redirect_to_https_port(struct http_connection *conn, int ssl_index) {
	char host[1025];
	int port;
	const char *host_header;
	char buf[64];

	if ((host_header = http_get_header(conn, "Host")) == NULL ||
			sscanf(host_header, "%1024[^:]", host) == 0) {
		/* No "Host:" header. Fallback to our IP address. */
		sockaddr_to_string(host, sizeof(host), &conn->socket.sockaddr);
	}
	port = conn->ctx->listening_sockets[ssl_index].sockaddr.sa_family == AF_INET ?
		(*(struct sockaddr_in *)
			&conn->ctx->listening_sockets[ssl_index].sockaddr).sin_port :
		conn->ctx->listening_sockets[ssl_index].sockaddr.sa_family == AF_INET6 ?
				(*(struct sockaddr_in6 *)
				 &conn->ctx->listening_sockets[ssl_index].sockaddr).sin6_port : 0;

	conn->status_code = 302;
	if (conn->ctx->config->server_software) {
		if (port == 443)
			http_printf(conn, "HTTP/1.1 302 Found\r\n"
				"Server: %s\r\n"
				"Date: %s\r\n"
				"Location: https://%s%s\r\n\r\n",
				conn->ctx->config->server_software,
				now(buf, sizeof(buf)),
				host, conn->request.uri);
		else
			http_printf(conn, "HTTP/1.1 302 Found\r\n"
				"Server: %s\r\n"
				"Date: %s\r\n"
				"Location: https://%s:%d%s\r\n\r\n",
				conn->ctx->config->server_software,
				now(buf, sizeof(buf)),
				host, ntohs(port), conn->request.uri);
	}
	else {
		if (port == 443)
			http_printf(conn, "HTTP/1.1 302 Found\r\n"
				"Date: %s\r\n"
				"Location: https://%s%s\r\n\r\n",
				now(buf, sizeof(buf)),
				host, conn->request.uri);
		else
			http_printf(conn, "HTTP/1.1 302 Found\r\n"
				"Date: %s\r\n"
				"Location: https://%s:%d%s\r\n\r\n",
				now(buf, sizeof(buf)),
				host, ntohs(port), conn->request.uri);
	}
}

static void send_options(struct http_connection *conn)
{
	char buf[64];

	conn->status_code = 200;
	if (conn->ctx->config->server_software)
 		http_printf(conn, "HTTP/1.1 200 OK\r\n"
			"Server: %s\r\n"
			"Date: %s\r\n"
			"Allow: GET, POST, HEAD, PUT, DELETE, OPTIONS\r\n"
			"DAV: 1\r\n\r\n",
			conn->ctx->config->server_software,
			now(buf, sizeof(buf)));
	else
 		http_printf(conn, "HTTP/1.1 200 OK\r\n"
			"Date: %s\r\n"
			"Allow: GET, POST, HEAD, PUT, DELETE, OPTIONS\r\n"
			"DAV: 1\r\n\r\n",
			now(buf, sizeof(buf)));
}

static void send_invalid_method(struct http_connection *conn)
{
	char buf[64];

	conn->status_code = 405;
	if (conn->ctx->config->server_software)
		http_printf(conn, "HTTP/1.1 405 Method Not Allowed\r\n"
			"Server: %s\r\n"
			"Date: %s\r\n"
			"Allow: GET, POST, HEAD, PUT, DELETE, OPTIONS\r\n\r\n",
			conn->ctx->config->server_software,
			now(buf, sizeof(buf)));
	else
		http_printf(conn, "HTTP/1.1 405 Method Not Allowed\r\n"
			"Date: %s\r\n"
			"Allow: GET, POST, HEAD, PUT, DELETE, OPTIONS\r\n\r\n",
			now(buf, sizeof(buf)));
}

/*
 * For given directory path, substitute it to valid index file. 
 * Return 1 if index file has been found, 0 if not found.  
 * If the file is found, it's mtime and size are updated..
 */

static int substitute_index_file(struct http_connection *conn,
				char *path, size_t pathlen, 
				int *is_dir, time_t *mtime, int64_t *size)
{
	size_t len = strlen(path);
	const char *list = conn->ctx->config->index_files;
	char *p;
	struct stat st;
	int found = 0;

	if (!list)
		return 0;

	while (len > 0 && path[len-1] == '/')
		len--;
	path[len] = '/';

	while ((p = strchrnul(list, ','))) {
		strlcpy(path+len+1, list, p-list+1);
		if (stat(path, &st) == 0 && !S_ISDIR(st.st_mode)) {
			*is_dir = 0;
			*mtime = st.st_mtime;
			*size = st.st_size;
			found = 1;
			return found;
		}
		if (*p == '\0')
			break;
		else list = p+1;
	}

	if (!found)
		path[len] = '\0';
	return found;
}

static void send_authorization_request(struct http_connection *conn)
{
	if (strcasecmp(conn->ctx->config->auth_type, "digest") == 0)
		send_digest_auth_request(conn);
	else
		send_basic_auth_request(conn);
}

static int check_authorization(struct http_connection *conn, const char *path)
{
	int authorized = 1;

	if (conn->ctx->config->protected_uri && 
			match(conn, &conn->ctx->protected_uri, conn->request.uri)) {
		if (strcasecmp(conn->ctx->config->auth_type, "digest") == 0)
			authorized = check_digest_auth(conn, path);
		else
			authorized = check_basic_auth(conn, path);
	}
	return authorized;
}

#if 0
static int is_authorized_for_put(struct http_connection *conn)
{
	const char *passfile = conn->ctx->config->auth_file;
	int ret = 0;

#if 0
	if (passfile != NULL && file_open(conn, passfile, "r", &file)) {
		ret = authorize(conn, &file);
		file_close(&file);
	}
#endif
	return ret;
}
#endif

static int rewrite(struct http_connection *conn, char *buf, size_t buflen)
{
	int i, j, code;
	regmatch_t match[9];
	int rewrited = 0;
	char *newbuf, *p, *q;
	size_t len = 0;

	for (i = 0; i < conn->ctx->config->nrewrite_rules; i++) {
		if ((code = regexec(&conn->ctx->rewrite_rules[i],
												buf, 9, match, 0)) != 0) {
			if (code == REG_NOMATCH)
				continue;
			else {
				char err[255];

				regerror(code, &conn->ctx->rewrite_rules[i], err, sizeof(err));
				http_error(conn, "regex failed matching \"%s\": %s", buf, err);
				continue;
			}
		}
		if (!(newbuf = malloc(buflen))) {
			http_error(conn, "malloc() failed for rewrite()");
			break;
		}
		p = conn->ctx->rewrite_rules_repl[i];
		q = newbuf;
		while (*p) {
			if (len >= buflen-1)
				continue;
			if (*p == '\\') {
				if (*(p+1) >= '1' && *(p+1) <= '9') {
					int idx = *(p+1)-'0';

					if (match[idx].rm_so != -1)
						for (j = match[idx].rm_so; j < match[idx].rm_eo; j++, len++)
							*q++ = buf[j];
					p += 2;
				}
				else {
					p++;
					*q++ = *p++, len++;
				}
			}
#if 0
			else if (*p == '&') {
				if (match[0].rm_so != -1)
					for (j = match[0].rm_so; j < match[0].rm_eo; j++, len++)
							*q++ = buf[j];
				p += 1;
			}
#endif
			else
				*q++ = *p++, len++;
		}
		*q = '\0';
		strcpy(buf, newbuf);
		free(newbuf);
		rewrited = 1;
	}
	return rewrited;
}

/* 
 * Convert URI to file name, applying rewrite rules if any.
 * Build CGI PATH_INFO variable
 */

static void convert_uri_to_file_name(struct http_connection *conn,
				char *buf, size_t buflen, int *is_dir, time_t *mtime, int64_t *size)
{
	struct stat st;

	*is_dir = 0;
	*mtime = 0;
	*size = 0;
	/* Using buflen-1 because memmove() for PATH_INFO may shift part 
	 * of the path one byte on the right.
	 */
	snprintf(buf, buflen-1, "%s%s",
					conn->ctx->config->docroot, conn->request.uri);
	http_trace(conn, "convert_to_file_name(%s)", buf);

	if (conn->ctx->config->nrewrite_rules > 0 && rewrite(conn, buf, buflen))
		http_trace(conn, "rewrite to %s", buf);

	if (stat(buf, &st) < 0) {
		/* Support PATH_INFO for CGI scripts.*/
		char * p;

		for (p = buf + strlen(buf); p > buf + 1; p--) {
			if (*p == '/') {
				*p = '\0';
				if (conn->ctx->config->cgi_pattern &&
						match(conn, &conn->ctx->cgi_pattern, buf) &&
						stat(buf, &st) == 0) {
					/* Shift PATH_INFO block one character right, e.g.  
					 * "/x.cgi/foo/bar\x00" => "/x.cgi\x00/foo/bar\x00" 
					 * conn->path_info is pointing to the local variable 
					 * "path" declared in handle_request(), so PATH_INFO 
					 * is not valid after handle_request returns.
					 */
					conn->path_info = p + 1;
					memmove(p+2, p+1, strlen(p+1)+1);/* +1 is for trailing \0*/
					p[1] = '/';
					*is_dir = S_ISDIR(st.st_mode);
					*mtime = st.st_mtime;
					*size = st.st_size;
					break;
				}
				else
					*p = '/';
			}
		}
	}
	else {
		*is_dir = S_ISDIR(st.st_mode);
		*mtime = st.st_mtime;
		*size = st.st_size;
	}
	http_trace(conn, "converted %sname %s, mtime=%ld, size=%" PRId64,
							*is_dir?"dir":"file", buf, *mtime, *size);
}

static int hide_files(struct http_connection *conn, const char *path)
{
	const char *p;

	if (!(p = strrchr(path, '/')))
		p = path;

	p++;

	return conn->ctx->config->hide_files && 
						match(conn, &conn->ctx->hide_files, p);
}


/*
 * This function is called when the request is read, parsed and validated,
 * and the server must decide what action to take: serve a file, or
 * a directory, or call embedded function, etcetera.
 */

void handle_request(struct http_connection *conn) {
	struct http_request *req = &conn->request;
	char path[PATH_MAX];
	int is_dir;
	time_t mtime;
	int64_t size;
	int uri_len, ssl_index;

	if ((conn->request.query_string = strchr(req->uri, '?')) != NULL) {
		*((char *)conn->request.query_string++) = '\0';
	}
	uri_len = strlen(req->uri);
	url_decode(req->uri, uri_len, (char *)req->uri, uri_len + 1, 0);
	url_normalize((char *)req->uri);
	convert_uri_to_file_name(conn, path, sizeof(path), &is_dir, &mtime, &size);

	http_trace(conn, "uri \"%s\" path \"%s\"", req->uri, path);

	if (!conn->socket.is_ssl &&
			conn->socket.ssl_redir &&
			(ssl_index = get_first_ssl_listener_index(conn->ctx)) > -1)
		redirect_to_https_port(conn, ssl_index);
	else if (conn->ctx->callbacks->begin_request &&
			 conn->ctx->callbacks->begin_request(conn))
		/* callback had done! */;
	else if (!is_put_or_delete_request(conn) &&
			 !check_authorization(conn, path))
		send_authorization_request(conn);
#if defined(USE_WEBSOCKET)
	else if (is_websocket_request(conn))
		handle_websocket_request(conn);
#endif
	else if (!is_acceptable_http_method(req->method))
		send_invalid_method(conn);
	else if (!strcmp(req->method, "OPTIONS"))
		send_options(conn);
	else if (!conn->ctx->config->docroot)
		http_send_response(conn, 404, "Not Found");
	else if (is_put_or_delete_request(conn) &&
			 conn->ctx->config->allow_update &&
			 !check_authorization(conn, path))
		send_authorization_request(conn);
	else if (strcmp(req->method, "PUT") == 0) {
		if (conn->ctx->config->allow_update)
			put_file(conn, path);
		else
			http_send_response(conn, 403, "Update Denied");
	}
	else if (strcmp(req->method, "DELETE") == 0) {
		if (conn->ctx->config->allow_update) {
			if (remove(path) == 0) {
				http_send_response(conn, 200, "OK");
			}
			else {
				http_error(conn, "Cannot delete \"%s\": %m", path);
				http_send_response(conn, 500, "Cannot Delete \"%s\"", path);
			}
		}
		else
			http_send_response(conn, 403, "Delete Denied");
	}
	else if (mtime == 0 || hide_files(conn, path))
		http_send_response(conn, 404, "Not Found");
	else if (is_dir && req->uri[uri_len - 1] != '/') {
		char buf[64];

		conn->status_code = 301;
		if (conn->ctx->config->server_software)
			http_printf(conn, "HTTP/1.1 301 Moved Permanently\r\n"
				"Server: %s\r\n"
				"Date: %s\r\n"
				"Location: %s/\r\n\r\n",
				conn->ctx->config->server_software,
				now(buf, sizeof(buf)),
				req->uri);
		else
			http_printf(conn, "HTTP/1.1 301 Moved Permanently\r\n"
				"Date: %s\r\n"
				"Location: %s/\r\n\r\n",
				now(buf, sizeof(buf)),
				req->uri);
	}
	else if (strcmp(req->method, "PROPFIND") == 0)
		handle_propfind(conn, path, is_dir, mtime, size);
	else if (is_dir &&
					 !substitute_index_file(conn, path, sizeof(path),
																 &is_dir, &mtime, &size)) {
		if (conn->ctx->config->allow_dirlist)
			handle_directory_request(conn, path);
		else
			http_send_response(conn, 403, "Directory Listing Denied");
	}
#if !defined(NO_CGI)
	else if (conn->ctx->config->cgi_pattern && 
					 match(conn, &conn->ctx->cgi_pattern, path)) {
		if (strcmp(req->method, "POST") != 0 &&
				strcmp(req->method, "HEAD") != 0 &&
				strcmp(req->method, "GET") != 0) 
			http_send_response(conn, 405, "Method %s Not Implemented"
													"\r\nAllow: GET, HEAD, POST", req->method);
		else
			handle_cgi_request(conn, path);
	}
#endif // !NO_CGI
	else if (conn->ctx->config->ssi_pattern && 
					 match(conn, &conn->ctx->ssi_pattern, path))
		handle_ssi_file_request(conn, path);
	else if (is_not_modified(conn, mtime, size))
		http_send_response(conn, 304, "Not Modified");
	else 
		handle_file_request(conn, path, mtime, size);
}

struct http_request *http_get_request(struct http_connection *conn)
{
	return &conn->request;
}

struct http_config *http_get_config(struct http_connection *conn)
{
	return conn->ctx->config;
}

void *http_get_user_data(struct http_connection *conn)
{
	return conn->ctx->user_data;
}

int http_get_var(const char *data, size_t datalen, const char *name,
				 char *dst, size_t dstlen)
{
	const char *p, *e, *s;
	size_t name_len;
	int len;

	if (dst == NULL || dstlen == 0)
		len = -2;
	else if (data == NULL || name == NULL || datalen == 0) {
		len = -1;
		dst[0] = '\0';
	}
	else {
		name_len = strlen(name);
		e = data + datalen;
		len = -1;
		dst[0] = '\0';

		/* data is "var1=val1&var2=val2...". Find variable first */
		for (p = data; p + name_len < e; p++) {
			if ((p == data || p[-1] == '&') && p[name_len] == '=' &&
					!strncasecmp(name, p, name_len)) {

				/* Point p to variable value */
				p += name_len + 1;

				/* Point s to the end of the value */
				s = (const char *) memchr(p, '&', (size_t)(e - p));
				if (s == NULL)
					s = e;
				assert(s >= p);

				/* Decode variable into destination buffer */
				len = url_decode(p, (size_t)(s - p), dst, dstlen, 1);

				/*
				 * Redirect error code from -1 to -2 (destination buffer
				 * too small).
				 */
				if (len == -1)
					len = -2;
				break;
			}
		}
	}
	return len;
}

int http_get_cookie(const char *cookie_header, const char *var_name,
									char *dst, size_t dstsize)
{
	const char *s, *p, *end;
	int namelen, len = -1;

	if (dst == NULL || dstsize == 0)
		len = -2;
	else if (var_name == NULL || (s = cookie_header) == NULL) {
		len = -1;
		dst[0] = '\0';
	}
	else {
		namelen = (int) strlen(var_name);
		end = s + strlen(s);
		dst[0] = '\0';

		for (; (s = strcasestr(s, var_name)) != NULL; s += namelen) {
			if (s[namelen] == '=') {
				s += namelen + 1;
				if ((p = strchr(s, ' ')) == NULL)
					p = end;
				if (p[-1] == ';')
					p--;
				if (*s == '"' && p[-1] == '"' && p > s + 1) {
					s++;
					p--;
				}
				if ((size_t) (p - s) < dstsize) {
					len = p - s;
					strlcpy(dst, s, (size_t) len + 1);
				} else
					len = -3;
				break;
			}
		}
	}
	return len;
}

