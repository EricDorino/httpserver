/*
 * Copyright (C) 2013, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fcntl.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include "internals.h"

/*FIXME*/

void http_close_connection(struct http_connection *conn)
{
#if 0
	if (conn->socket_ssl_ctx != NULL)
		SSL_CTX_free((SSL_CTX *) conn->socket_ssl_ctx);
#else
#endif
	close_connection(conn);
	free(conn);
}

struct http_connection *http_connect(const char *host, int port, int use_ssl,
								 char *ebuf, size_t ebuf_len)
{
	static struct http_context fake_ctx;
	struct http_connection *conn = NULL;
	struct sockaddr_in sin;
	struct hostent *he;
	int sock;

	if (host == NULL)
		snprintf(ebuf, ebuf_len, "%s", "NULL host");
	else if ((he = gethostbyname(host)) == NULL)
		snprintf(ebuf, ebuf_len, "gethostbyname(%s): %m", host);
	else if ((sock = socket(PF_INET, SOCK_STREAM, 0)) < 0)
		snprintf(ebuf, ebuf_len, "socket(): %m");
	else {
		sin.sin_family = AF_INET;
		sin.sin_port = htons((uint16_t) port);
		sin.sin_addr = * (struct in_addr *) he->h_addr_list[0];
		if (connect(sock, (struct sockaddr *) &sin, sizeof(sin)) != 0) {
			snprintf(ebuf, ebuf_len, "connect(%s:%d): %m", host, port);
			close(sock);
		}
		else if ((conn = (struct http_connection *)
						calloc(1, sizeof(*conn) + MAX_REQUEST_SIZE)) == NULL) {
			snprintf(ebuf, ebuf_len, "calloc(): %m");
			close(sock);
		}
#ifndef NO_SSL
#if 0
		else if (use_ssl && (conn->client_ssl_ctx =
							 SSL_CTX_new(SSLv23_client_method())) == NULL) {
			snprintf(ebuf, ebuf_len, "SSL_CTX_new error");
			close(sock);
			free(conn);
			conn = NULL;
		}
#else
#endif
#endif // NO_SSL
		else {
			conn->buf_size = MAX_REQUEST_SIZE;
			conn->buf = (char *) (conn + 1);
			conn->ctx = &fake_ctx;
			conn->socket.fd = sock;
			memcpy(&conn->socket.sockaddr, &sin, conn->socket.socklen = sizeof(sin));
			conn->socket.is_ssl = use_ssl;
#ifndef NO_SSL
#if 0
			if (use_ssl) {
				/*
				 * SSL_CTX_set_verify call is needed to switch off server 
				 * certificate checking, which is off by default in OpenSSL 
				 * and on in yaSSL.
				 */
				SSL_CTX_set_verify(conn->client_ssl_ctx, 0, 0);
				sslize(conn, conn->client_ssl_ctx, SSL_connect);
			}
#else
#endif
#endif
		}
	}
	return conn;
}

struct http_connection *http_download(const char *host, int port, int use_ssl,
							char *buf, size_t buflen, const char *fmt, ...)
{
	struct http_connection *conn;
	va_list ap;

	va_start(ap, fmt);

	buf[0] = '\0';

	if (!(conn = http_connect(host, port, use_ssl, buf, buflen))) {
		/* Nothing */
	}
	else if (http_vprintf(conn, fmt, ap) <= 0)
		strncpy(buf, "Error sending request", buflen);
	else
		get_request(conn, buf, buflen);

	if (buf[0] != '\0' && conn) {
		http_close_connection(conn);
		conn = NULL;
	}
	return conn;
}

