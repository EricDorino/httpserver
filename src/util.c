/*
 * Copyright (C) 2013, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "internals.h"

/*
 * Math str toward regex.
 */

int match(struct http_connection *conn, regex_t *preg, const char *str)
{
	int code;

	if ((code = regexec(preg, str, 0, NULL, 0)) != 0) {
		if (code == REG_NOMATCH)
			return 0;
		else {
			char err[255];

			regerror(code, preg, err, sizeof(err));
			http_error(conn, "regex failed matching \"%s\": %s", str, err);
			return 0;
		}
	}
	return 1;
}

/*
 * Skip the characters until one of the delimiters characters found.
 * 0-terminate resulting word. Skip the delimiter and following whitespaces.
 * Advance pointer to buffer to the next word. Return found 0-terminated word.
 * Delimiters can be quoted with quotechar.
 */

char *skip_quoted(char **buf, const char *delimiters,
						 const char *whitespace, char quotechar)
{
	char *p, *begin_word, *end_word, *end_whitespace;

	begin_word = *buf;
	end_word = begin_word + strcspn(begin_word, delimiters);

	/* Check for quotechar */
	if (end_word > begin_word) {
		p = end_word - 1;
		while (*p == quotechar) {
			/* If there is anything beyond end_word, copy it */
			if (*end_word == '\0') {
				*p = '\0';
				break;
			}
			else {
				size_t end_off = strcspn(end_word + 1, delimiters);
				memmove (p, end_word, end_off + 1);
				p += end_off; /* p must correspond to end_word - 1 */
				end_word += end_off + 1;
			}
		}
		for (p++; p < end_word; p++)
			*p = '\0';
	}

	if (*end_word == '\0')
		*buf = end_word;
	else {
		end_whitespace = end_word + 1 + strspn(end_word + 1, whitespace);

		for (p = end_word; p < end_whitespace; p++)
			*p = '\0';

		*buf = end_whitespace;
	}
	return begin_word;
}

/*
 * Simplified version of skip_quoted without quote char
 * and whitespace == delimiters
 */

char *skip(char **buf, const char *delimiters)
{
	return skip_quoted(buf, delimiters, delimiters, 0);
}

char *url_encode(const char *src,
				char *dst, size_t dstlen, 
				const char *dont_escape2) {
	static const char *dont_escape = "._-$,;~()";
	static const char *hex = "0123456789abcdef";
	char *ret = dst;
	const char *end = dst + dstlen-1;

	for (; *src != '\0' && dst < end; src++, dst++) {
		if (isalnum(*src) ||
				strchr(dont_escape, *src) != NULL ||
				(dont_escape2  && strchr(dont_escape2, *src) != NULL))
			*dst = *src;
		else if (dst + 2 < end) {
			dst[0] = '%';
			dst[1] = hex[(*src) >> 4];
			dst[2] = hex[(*src) & 0xf];
			dst += 2;
		}
	}
	*dst = '\0';
	return ret;
}

/*
 * Decode input buffer into destination buffer.
 * Returns the length of decoded data.
 * Form-url-encoded data differs from URI encoding in a way that it 
 * uses '+' as character for space, see RFC 1866 section 8.2.1 
 * http://ftp.ics.uci.edu/pub/ietf/html/rfc1866.txt
 */

#define hextoi(x) (isdigit(x) ? (x)-'0' : (x)-'a'+10)

int url_decode(const char *src, int srclen, char *dst,
					int dstlen, int is_form_url_encoded) {
	int i, j, a, b;

	for (i = j = 0; i < srclen && j < dstlen - 1; i++, j++) {
		if (src[i] == '%' && i < srclen - 2 &&
				isxdigit(*(src+i+1)) &&
				isxdigit(*(src+i+2))) {
			a = tolower(*(src+i+1));
			b = tolower(*(src+i+2));
			dst[j] = (hextoi(a) << 4) | hextoi(b);
			i += 2;
		}
		else if (is_form_url_encoded && src[i] == '+')
			dst[j] = ' ';
		else
			dst[j] = src[i];
	}
	dst[j] = '\0';
	return i >= srclen ? j : -1;
}

/*
 * Protect against directory disclosure attack by removing '..',
 * excessive '/' and '\' characters
 */

void url_normalize(char *str)
{
	char *p = str;

	while (str && *str) {
		*p++ = *str++;
		if (str[-1] == '/' || str[-1] == '\\') {
			while (str[0]) {
				if (str[0] == '/' || str[0] == '\\')
					str++;
				else if (str[0] == '.' && str[1] == '.')
					str += 2;
		 		else
					break;
			}
		}
	}
	*p = '\0';
}

/*
 * Date functions
 */

void gmt_date_string(char *buf, size_t buflen, time_t *t)
{
	strftime(buf, buflen, "%a, %d %b %Y %T %Z", gmtime(t));
}

char *now(char *buf, size_t buflen)
{
	time_t n = time(NULL);

	gmt_date_string(buf, buflen, &n);
	return buf;
}

time_t parse_date_string(const char *buf)
{
	time_t result;

	if (parse_date(buf, &result, NULL) < 0)
		result = (time_t)-1;
	return result;
}

/*
 * Update connection information
 */

void http_set_status_code(struct http_connection *conn, int status_code)
{
	conn->status_code = status_code;
}

void http_must_close(struct http_connection *conn)
{
	conn->must_close = 1;
}

void http_add_bytes_sent(struct http_connection *conn, int bytes_sent)
{
	conn->bytes_sent += bytes_sent;
}

