/*
 * Copyright (C) 2018, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "httpserverxx.h"
#include "c_connector.h"

http::server *http::server::_server = nullptr;

#ifdef __cplusplus
extern "C" {
#endif

int CB_begin_request(struct http_connection *conn) {
	return http::server::instance()->begin_request(conn);
}

void CB_end_request(const struct http_connection *conn, int status_code) {
	http::server::instance()->end_request(conn, status_code);
}

void CB_log_message(const struct http_connection *conn,
								    int priority, const char *message, va_list args) {
	http::server::instance()->log_message(conn, priority, message, args);
}

int CB_websocket_connect(const struct http_connection *c) {
	return http::server::instance()->websocket_connect(c);
}

void CB_websocket_ready(struct http_connection *conn) {
	http::server::instance()->websocket_ready(conn);
}

int CB_websocket_data(struct http_connection *conn,
								int bits, char *data, size_t len) {
	return http::server::instance()->websocket_data(conn, bits, data, len);
}

void CB_upload(struct http_connection *conn, const char *filename) {
	http::server::instance()->upload(conn, filename);
}

int CB_http_response(struct http_connection *conn,
								int status, const char *message, va_list args) {
	return http::server::instance()->http_response(conn, status, message, args);
}

int CB_local_gcry_init(struct http_context *ctx) {
	return http::server::instance()->local_gcry_init(ctx);
}

#ifdef __cplusplus
}
#endif
