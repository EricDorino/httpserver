/*
 * Copyright (C) 2018, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef c_connector_h_
#define c_connector_h_

#include "httpserver.h"

#ifdef __cplusplus
extern "C" {
#endif

int CB_begin_request(struct http_connection *);
void CB_end_request(const struct http_connection *, int);
void CB_log_message(const struct http_connection *, int, const char *, va_list);
int CB_websocket_connect(const struct http_connection *);
void CB_websocket_ready(struct http_connection *);
int CB_websocket_data(struct http_connection *, int, char *, size_t);
void CB_upload(struct http_connection *, const char *);
int CB_http_response(struct http_connection *, int, const char *, va_list);
int CB_local_gcry_init(struct http_context *);

#ifdef __cplusplus
}
#endif

#endif
