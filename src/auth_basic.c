/*
 * Copyright (C) 2013, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/wait.h>
#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include "internals.h"
#define BUFLEN	8192

/*
 * Basic authorization.
 */

#define is_upper(c) ((c)>='A' && (c)<='Z')
#define is_lower(c) ((c)>='a' && (c)<='z')
#define is_digit(c) ((c)>='0' && (c)<='9')

static char *base64_decode(char *dst, size_t size, const char *src)
{
	unsigned char in[4] = {0, 0, 0, 0}; /* RATS: ignore */
	unsigned char out[3] = {0, 0, 0}; /* RATS: ignore */
	int i, j, index;
	char *p;
	const char *plast;

	if (!src)
		return NULL;

	for (plast = (char *)src; *plast != '=' && *plast != '\0'; plast++);
	plast--;

	for (p = (char *)src, i = 0, j = 0; p <= plast; p++, i++) {
		index = i % 4;

		if (is_upper(*p)) in[index] = *p - 65;
		else if (is_lower(*p)) in[index] = *p - 71;
		else if (is_digit(*p)) in[index] = *p + 4;
		else if (*p == '+') in[index] = 62;
		else if (*p == '/') in[index] = 63;
		else if (*p == '\r' || *p == '\n') { i--; continue;}
		else in[index] = 0;

		if (index == 3 || p == plast) {
			out[0] = (in[0] << 2) | ((in[1] & 0x30) >> 4);
			out[1] = ((in[1] & 0x0f) << 4) | ((in[2] & 0x3c) >> 2);
			out[2] = ((in[2] & 0x03) << 6) | (in[3] & 0x3f);

			if (j >= (int)size - 4)
				break;
			if (p == plast) {
				dst[j++] = out[0];
				index >= 2 ? dst[j++] = out[1] : 0;
				index == 3 ? dst[j++] = out[2] : 0;
			}
			else {
				dst[j++] = out[0];
				dst[j++] = out[1];
				dst[j++] = out[2];
			}
			out[0] = out[1] = out[2] = 0;
			in[0] = in[1] = in[2] = in[3] = 0;
		}
	}
	dst[j] = '\0';

	return dst;
}

struct auth_header {
	char *user, *password;
};

static int parse_basic_auth_header(struct http_connection *conn,
				char *buf, size_t bufsize, struct auth_header *ah)
{
	const char *header;
	char *p;

	memset(ah, 0, sizeof(*ah));
  if (!(header = http_get_header(conn, "Authorization")) ||
			strncasecmp(header, "Basic ", 6) != 0)
		return 0;

	base64_decode(buf, bufsize, header + 6);
	p = buf;
	
	while (isspace(*p))
		p++;
	ah->user = p;
	while (*p != ':')
		p++;
	*p++ = '\0';
	ah->password = p;

	if (ah->user)
		conn->request.remote_user = strdup(ah->user);
	else
		return 0;
	return 1;
}

const char *http_basic_auth_password(struct http_connection *conn)
{
	char buf[BUFLEN];
	struct auth_header ah;

	if (parse_basic_auth_header(conn, buf, sizeof(buf), &ah)) {
		conn->request.remote_user = strdup(ah.user);
		return strdup(ah.password);
	}
	return NULL;
}

static void childreaper(int sig) {}

int check_basic_auth(struct http_connection *conn, const char *path)
{
	char buf[BUFLEN];
	struct auth_header ah;
	char * const args[2] = {(char *)conn->ctx->config->auth_program, NULL};
	pid_t pid;
	int fd_stdin[2], fd_stdout[2];
	FILE *in, *out;
	sighandler_t sigchld;
	int ret, status;

	if (!parse_basic_auth_header(conn, buf, sizeof(buf), &ah))
		return 0;

	fd_stdin[0] = fd_stdin[1] = fd_stdout[0] = fd_stdout[1] = -1;
	in = out = NULL;

	if (pipe(fd_stdin) < 0 || pipe(fd_stdout) < 0) {
		http_error(conn, "auth pipe(): %m");
		return 0;
	}

	sigchld = signal(SIGCHLD, childreaper);

	pid = http_spawn_process(conn, args, NULL, fd_stdin[0], fd_stdout[1], "/tmp");
	fd_stdin[0] = fd_stdout[1] = -1;

	if (pid < 0)
		return 0;

	if (!(in = fdopen(fd_stdin[1], "wb")) ||
			!(out = fdopen(fd_stdout[0], "rb"))) {
		http_error(conn, "fdopen: %m");
		return 0;
	}
	setbuf(in, NULL);
	setbuf(out, NULL);

	fprintf(in, "%s\n%s\n", ah.user, ah.password);

	if (waitpid(pid, &status, 0) < 0) {
		http_error(conn, "waitpid(): %m");
		ret = 0;
		goto done;
	}

	signal(SIGCHLD, sigchld);

	ret = WIFEXITED(status) && WEXITSTATUS(status) == 0;

	http_trace(conn, "auth basic status=%d ret=%d", WEXITSTATUS(status), ret);

done:
	fclose(in);
	fclose(out);
	return ret;
}

void send_basic_auth_request(struct http_connection *conn)
{
	char buf[64];

	conn->status_code = 401;
	if (conn->ctx->config->server_software)
		http_printf(conn,
			"HTTP/1.1 401 Unauthorized\r\n"
			"Server: %s\r\n"
			"Date: %s\r\n"
			"WWW-Authenticate: Basic realm=\"%s\"\r\n\r\n",
			conn->ctx->config->server_software,
			now(buf, sizeof(buf)),
			conn->ctx->config->domain);
	else
		http_printf(conn,
			"HTTP/1.1 401 Unauthorized\r\n"
			"Date: %s\r\n"
			"WWW-Authenticate: Basic realm=\"%s\"\r\n\r\n",
			now(buf, sizeof(buf)),
			conn->ctx->config->domain);
}
