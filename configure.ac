
AC_INIT([httpserver], [1.0.0], [eric.dorino@gmail.com], [httpserver])

AC_CANONICAL_BUILD
AC_CANONICAL_HOST
AC_CANONICAL_TARGET
AM_INIT_AUTOMAKE

AC_PREFIX_DEFAULT(/usr/local)

AC_CONFIG_SRCDIR([README])
AC_CONFIG_MACRO_DIR([m4])

# Options
AC_ARG_ENABLE(debug,
		AS_HELP_STRING([--disable-debug],
					   [disable debugging information]),
		USE_DEBUG="$enableval", USE_DEBUG="yes")

case "$USE_DEBUG" in
"yes")	DEBUG_CFLAGS="-g -W -Wall -Wno-unused -Wextra "
		DEBUG_CPPFLAGS="-DDEBUG "
		DEBUG_YFLAGS="-vt "
		;;
*)		DEBUG_CFLAGS="-02 -W -Wall -Wno-unused -Wextra "
		DEBUG_CPPFLAGS="-DNDEBUG "
		DEBUG_YFLAGS=" "
		;;
esac

# Checks for programs.
AC_PROG_CC
AC_PROG_CXX
AC_PROG_YACC
AC_PROG_LIBTOOL
AC_PROG_INSTALL
AC_PROG_LN_S
AC_PROG_MAKE_SET
AC_CHECK_PROG(PDFLATEX,pdflatex,pdflatex)
if test -z "$PDFLATEX"; then
	AC_MSG_WARN([Unable to create PDF version of documentation.])
fi
AM_CONDITIONAL([HAVE_PDFLATEX], test -n "$PDFLATEX")
AM_PROG_CC_C_O
PKG_PROG_PKG_CONFIG

# Check for guile
AC_ARG_WITH([guile],
	AS_HELP_STRING([--with-guile],
		[embed a guile interpreter @<:@default is to check@:>@]),
		[use_guile="$withval"],
		[use_guile="check"])

if test "x$use_guile" != "xno"; then
	PKG_CHECK_MODULES(GUILE, [guile-2.0],
		AC_DEFINE([USE_GUILE], [1], ["Define if you have guile(1)"]),
		[if test "x$use_guile" != "xcheck"; then
			AC_MSG_FAILURE(["No guile(1) available"])
		fi],)
fi

# Checks for libraries.
AX_PTHREAD(PTHREAD_LIBS="-pthread",
	AC_MSG_ERROR(["Dynamic library (pthread) not found"]))
AM_PATH_LIBGCRYPT(,,AC_MSG_FAILURE(["No libgcrypt available"]))
PKG_CHECK_MODULES(GNUTLS, [gnutls],,AC_MSG_FAILURE(["No gnutls available"]))

# Checks for header files.
AC_STDC_HEADERS
AC_CHECK_HEADERS([gcrypt.h])

# Checks for typedefs, structures, and compiler characteristics.

# Checks for library functions.

# Setup compile/link flags

CURRENT=1
REVISION=0
AGE=0
VERSION_INFO="$CURRENT:$REVISION:$AGE"

CPPFLAGS="$CPPFLAGS $GNUTLS_CFLAGS $DEBUG_CPPFLAGS "
CFLAGS="$CFLAGS $DEBUG_CFLAGS "
YFLAGS="$YFLAGS $DEBUG_YFLAGS "
LIBS="$LIBS $PTHREAD_LIBS $GNUTLS_LIBS "
AC_SUBST(CPPFLAGS)
AC_SUBST(CFLAGS)
AC_SUBST(LIBS)

AC_SUBST(LIBGCRYPT_CFLAGS)
AC_SUBST(LIBGCRYPT_LIBS)
AC_SUBST(GUILE_CFLAGS)
AC_SUBST(GUILE_LIBS)
AC_SUBST(VERSION_INFO)

AC_OUTPUT([ \
	Makefile \
	src/Makefile \
	src/httpserver.pc \
])
